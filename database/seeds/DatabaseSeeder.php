php <?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for($i = 0; $i < 10; $i++) {
            App\Admin::create([
                'name' => $faker->name,
                'email' => $faker->email,
                'password' => bcrypt('123456')
            ]);
        }
    }
}
