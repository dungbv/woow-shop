@extends('layouts.app')
@section('title')
    Cart
    @endsection
@section('content')
    @component('layouts.client-partial.breadcrumb_content')
        @slot('curent_page')
            Cart
        @endslot
    @endcomponent
    <div class="content-container">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="main-content">
                        <div class="commerce">
                            <p class="cart-empty">Your cart is currently empty.</p>
                            <p class="return-to-shop"><a class="button wc-backward" href="#">Return To Shop</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection