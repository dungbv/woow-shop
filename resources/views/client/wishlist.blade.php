@extends('layouts.app')
@section('title')
    Wishlist
@endsection
@section('content')
    @component('layouts.client-partial.breadcrumb_content')
        @slot('curent_page')
            Wishlist
        @endslot
    @endcomponent
    <div class="content-container">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="main-content">
                        <form class="commerce">
                            <div class="wishlist-title ">
                                <h2>My wishlist on WooW</h2>
                            </div>
                            <table class="shop_table cart wishlist_table">
                                <thead>
                                <tr>
                                    <th class="product-remove"></th>
                                    <th class="product-thumbnail"></th>
                                    <th class="product-name"><span class="nobr">Product Name</span></th>
                                    <th class="product-price"><span class="nobr">Unit Price </span></th>
                                    <th class="product-stock-stauts"><span class="nobr">Stock Status </span></th>
                                    <th class="product-add-to-cart"></th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td class="product-remove">
                                        <a href="#" class="remove remove_from_wishlist">&times;</a>
                                    </td>
                                    <td class="product-thumbnail">
                                        <a href="shop-detail-1.html">
                                            <img width="100" height="150" src="{{ asset('client/images/products/product_80x80.jpg') }}" alt="Product-1"/>
                                        </a>
                                    </td>
                                    <td class="product-name">
                                        <a href="shop-detail-1.html">Cras rhoncus duis viverra</a>
                                    </td>
                                    <td class="product-price">
													<span class="amount">
														&#36;12.00</span>&ndash;<span class="amount">&#36;23.00
													</span>
                                    </td>
                                    <td class="product-stock-status">
                                        <span class="wishlist-in-stock">In Stock</span>
                                    </td>
                                    <td class="product-add-to-cart">
                                        <a href="#" class="add_to_cart_button button"> Add to cart</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="product-remove">
                                        <a href="#" class="remove remove_from_wishlist">&times;</a>
                                    </td>
                                    <td class="product-thumbnail">
                                        <a href="shop-detail-1.html">
                                            <img width="100" height="150" src="{{ asset('client/images/products/product_80x80.jpg') }}" alt="Product-3"/>
                                        </a>
                                    </td>
                                    <td class="product-name">
                                        <a href="shop-detail-1.html">Creamy Spring Pasta</a>
                                    </td>
                                    <td class="product-price">
													<span class="amount">
														&#36;12.00</span>&ndash;<span class="amount">&#36;23.00
													</span>
                                    </td>
                                    <td class="product-stock-status">
                                        <span class="wishlist-in-stock">In Stock</span>
                                    </td>
                                    <td class="product-add-to-cart">
                                        <a href="#" class="add_to_cart_button button"> Add to cart</a>
                                    </td>
                                </tr>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="6">&nbsp;</td>
                                </tr>
                                </tfoot>
                            </table>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection