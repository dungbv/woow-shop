@extends('layouts.app')
@section('title')
    Cart
@endsection
@section('content')
    @component('layouts.client-partial.breadcrumb_content')
        @slot('curent_page')
            Cart
        @endslot
    @endcomponent
    <div class="content-container">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="main-content">
                        <div class="row row-fluid mb-4">
                            <div class="col-sm-2"></div>
                            <div class="col-sm-8">
                                <h2 class="text-center">CHAIRS</h2>
                                <div class="separator separator_align_center sep_width_10 sep_border_width_2 sep_pos_align_center separator_no_text sep_color_black mb-4">
											<span class="sep_holder sep_holder_l">
												<span class="sep_line"></span>
											</span>
                                    <span class="sep_holder sep_holder_r">
												<span class="sep_line"></span>
											</span>
                                </div>
                                <p class="text-center">
                                    The Women Collection does its job from a foundation of strength and function.Cras eu venenatis enim, sed facilisis leo. Sed lobortis egestas semper. Ut dapibus lacus sapien, nec tristique justo consequat dapibus. Aenean bibendum.
                                </p>
                            </div>
                            <div class="col-sm-2"></div>
                        </div>
                        <div class="caroufredsel product-slider" data-height="variable" data-visible-min="1" data-responsive="1" data-infinite="1" data-autoplay="0">
                            <div class="caroufredsel-wrap">
                                <div class="commerce columns-4">
                                    <ul class="products columns-4" data-columns="4">
                                        <li class="product product-no-border style-2">
                                            <div class="product-container">
                                                <figure>
                                                    <div class="product-wrap">
                                                        <div class="product-images">
                                                            <span class="onsale">Sale!</span>
                                                            <div class="shop-loop-thumbnail shop-loop-front-thumbnail">
                                                                <a href="shop-detail-1.html"><img width="450" height="450" src="{{ asset('client/images/products/product_263x263.jpg') }}" alt=""/></a>
                                                            </div>
                                                            <div class="shop-loop-thumbnail shop-loop-back-thumbnail">
                                                                <a href="shop-detail-1.html"><img width="450" height="450" src="{{ asset('client/images/products/product_263x263alt.jpg') }}" alt=""/></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <figcaption>
                                                        <div class="shop-loop-product-info">
                                                            <div class="info-meta clearfix">
                                                                <div class="star-rating">
                                                                    <span style="width:0%"></span>
                                                                </div>
                                                                <div class="loop-add-to-wishlist">
                                                                    <div class="yith-wcwl-add-to-wishlist">
                                                                        <div class="yith-wcwl-add-button">
                                                                            <a href="#" class="add_to_wishlist">
                                                                                Add to Wishlist
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="info-content-wrap">
                                                                <h3 class="product_title">
                                                                    <a href="shop-detail-1.html">Daniel Stromborg Round</a>
                                                                </h3>
                                                                <div class="info-price">
																			<span class="price">
																				<del><span class="amount">£23.00</span></del> <ins><span class="amount">£20.00</span></ins>
																			</span>
                                                                </div>
                                                                <div class="loop-action">
                                                                    <div class="loop-add-to-cart">
                                                                        <a href="#" class="add_to_cart_button">
                                                                            Add to cart
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </figcaption>
                                                </figure>
                                            </div>
                                        </li>
                                        <li class="product product-no-border style-2">
                                            <div class="product-container">
                                                <figure>
                                                    <div class="product-wrap">
                                                        <div class="product-images">
                                                            <div class="shop-loop-thumbnail shop-loop-front-thumbnail">
                                                                <a href="shop-detail-1.html"><img width="450" height="450" src="{{ asset('client/images/products/product_263x263.jpg') }}" alt=""/></a>
                                                            </div>
                                                            <div class="shop-loop-thumbnail shop-loop-back-thumbnail">
                                                                <a href="shop-detail-1.html"><img width="450" height="450" src="{{ asset('client/images/products/product_263x263alt.jpg') }}" alt=""/></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <figcaption>
                                                        <div class="shop-loop-product-info">
                                                            <div class="info-meta clearfix">
                                                                <div class="star-rating">
                                                                    <span style="width:0%"></span>
                                                                </div>
                                                                <div class="loop-add-to-wishlist">
                                                                    <div class="yith-wcwl-add-to-wishlist">
                                                                        <div class="yith-wcwl-add-button">
                                                                            <a href="#" class="add_to_wishlist">
                                                                                Add to Wishlist
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="info-content-wrap">
                                                                <h3 class="product_title">
                                                                    <a href="shop-detail-1.html">Hans Wegner Shell Chair</a>
                                                                </h3>
                                                                <div class="info-price">
																			<span class="price">
																				<span class="amount">&pound;10.75</span>
																			</span>
                                                                </div>
                                                                <div class="loop-action">
                                                                    <div class="loop-add-to-cart">
                                                                        <a href="#" class="add_to_cart_button">
                                                                            Add to cart
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </figcaption>
                                                </figure>
                                            </div>
                                        </li>
                                        <li class="product product-no-border style-2">
                                            <div class="product-container">
                                                <figure>
                                                    <div class="product-wrap">
                                                        <div class="product-images">
                                                            <span class="onsale">Sale!</span>
                                                            <div class="shop-loop-thumbnail shop-loop-front-thumbnail">
                                                                <a href="shop-detail-1.html"><img width="450" height="450" src="{{ asset('client/images/products/product_263x263.jpg') }}" alt=""/></a>
                                                            </div>
                                                            <div class="shop-loop-thumbnail shop-loop-back-thumbnail">
                                                                <a href="shop-detail-1.html"><img width="450" height="450" src="{{ asset('client/images/products/product_263x263alt.jpg') }}" alt=""/></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <figcaption>
                                                        <div class="shop-loop-product-info">
                                                            <div class="info-meta clearfix">
                                                                <div class="star-rating">
                                                                    <span style="width:0%"></span>
                                                                </div>
                                                                <div class="loop-add-to-wishlist">
                                                                    <div class="yith-wcwl-add-to-wishlist">
                                                                        <div class="yith-wcwl-add-button">
                                                                            <a href="#" class="add_to_wishlist">
                                                                                Add to Wishlist
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="info-content-wrap">
                                                                <h3 class="product_title">
                                                                    <a href="shop-detail-1.html">Hans Wegner Two Seat Sofa</a>
                                                                </h3>
                                                                <div class="info-price">
																			<span class="price">
																				<del><span class="amount">£20.50</span></del>
																				<ins><span class="amount">£19.00</span></ins>
																			</span>
                                                                </div>
                                                                <div class="loop-action">
                                                                    <div class="loop-add-to-cart">
                                                                        <a href="#" class="add_to_cart_button">
                                                                            Add to cart
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </figcaption>
                                                </figure>
                                            </div>
                                        </li>
                                        <li class="product product-no-border style-2">
                                            <div class="product-container">
                                                <figure>
                                                    <div class="product-wrap">
                                                        <div class="product-images">
                                                            <span class="onsale">Sale!</span>
                                                            <div class="shop-loop-thumbnail shop-loop-front-thumbnail">
                                                                <a href="shop-detail-1.html"><img width="450" height="450" src="{{ asset('client/images/products/product_263x263.jpg') }}" alt=""/></a>
                                                            </div>
                                                            <div class="shop-loop-thumbnail shop-loop-back-thumbnail">
                                                                <a href="shop-detail-1.html"><img width="450" height="450" src="{{ asset('client/images/products/product_263x263alt.jpg') }}" alt=""/></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <figcaption>
                                                        <div class="shop-loop-product-info">
                                                            <div class="info-meta clearfix">
                                                                <div class="star-rating">
                                                                    <span style="width:0%"></span>
                                                                </div>
                                                                <div class="loop-add-to-wishlist">
                                                                    <div class="yith-wcwl-add-to-wishlist">
                                                                        <div class="yith-wcwl-add-button">
                                                                            <a href="#" class="add_to_wishlist">
                                                                                Add to Wishlist
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="info-content-wrap">
                                                                <h3 class="product_title">
                                                                    <a href="shop-detail-1.html">Hans Wegner Desk</a>
                                                                </h3>
                                                                <div class="info-price">
																			<span class="price">
																				<del><span class="amount">£20.50</span></del>
																				<ins><span class="amount">£19.00</span></ins>
																			</span>
                                                                </div>
                                                                <div class="loop-action">
                                                                    <div class="loop-add-to-cart">
                                                                        <a href="#" class="add_to_cart_button">
                                                                            Add to cart
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </figcaption>
                                                </figure>
                                            </div>
                                        </li>
                                        <li class="product product-no-border style-2">
                                            <div class="product-container">
                                                <figure>
                                                    <div class="product-wrap">
                                                        <div class="product-images">
                                                            <div class="shop-loop-thumbnail shop-loop-front-thumbnail">
                                                                <a href="shop-detail-1.html"><img width="450" height="450" src="{{ asset('client/images/products/product_263x263.jpg') }}" alt=""/></a>
                                                            </div>
                                                            <div class="shop-loop-thumbnail shop-loop-back-thumbnail">
                                                                <a href="shop-detail-1.html"><img width="450" height="450" src="{{ asset('client/images/products/product_263x263alt.jpg') }}" alt=""/></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <figcaption>
                                                        <div class="shop-loop-product-info">
                                                            <div class="info-meta clearfix">
                                                                <div class="star-rating">
                                                                    <span style="width:0%"></span>
                                                                </div>
                                                                <div class="loop-add-to-wishlist">
                                                                    <div class="yith-wcwl-add-to-wishlist">
                                                                        <div class="yith-wcwl-add-button">
                                                                            <a href="#" class="add_to_wishlist">
                                                                                Add to Wishlist
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="info-content-wrap">
                                                                <h3 class="product_title">
                                                                    <a href="shop-detail-1.html">Florence Knoll Executive</a>
                                                                </h3>
                                                                <div class="info-price">
																			<span class="price">
																				<del><span class="amount">£20.50</span></del>
																				<ins><span class="amount">£19.00</span></ins>
																			</span>
                                                                </div>
                                                                <div class="loop-action">
                                                                    <div class="loop-add-to-cart">
                                                                        <a href="#" class="add_to_cart_button">
                                                                            Add to cart
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </figcaption>
                                                </figure>
                                            </div>
                                        </li>
                                        <li class="product product-no-border style-2">
                                            <div class="product-container">
                                                <figure>
                                                    <div class="product-wrap">
                                                        <div class="product-images">
                                                            <div class="shop-loop-thumbnail shop-loop-front-thumbnail">
                                                                <a href="shop-detail-1.html"><img width="450" height="450" src="{{ asset('client/images/products/product_263x263.jpg') }}" alt=""/></a>
                                                            </div>
                                                            <div class="shop-loop-thumbnail shop-loop-back-thumbnail">
                                                                <a href="shop-detail-1.html"><img width="450" height="450" src="{{ asset('client/images/products/product_263x263alt.jpg') }}" alt=""/></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <figcaption>
                                                        <div class="shop-loop-product-info">
                                                            <div class="info-meta clearfix">
                                                                <div class="star-rating">
                                                                    <span style="width:80%"></span>
                                                                </div>
                                                                <div class="loop-add-to-wishlist">
                                                                    <div class="yith-wcwl-add-to-wishlist">
                                                                        <div class="yith-wcwl-add-button">
                                                                            <a href="#" class="add_to_wishlist">
                                                                                Add to Wishlist
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="info-content-wrap">
                                                                <h3 class="product_title">
                                                                    <a href="shop-detail-1.html">Citterio Grand Repos</a>
                                                                </h3>
                                                                <div class="info-price">
																			<span class="price">
																				<span class="amount">£12.00</span>
																				–
																				<span class="amount">£20.00</span>
																			</span>
                                                                </div>
                                                                <div class="loop-action">
                                                                    <div class="loop-add-to-cart">
                                                                        <a href="#" class="add_to_cart_button">
                                                                            Add to cart
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </figcaption>
                                                </figure>
                                            </div>
                                        </li>
                                        <li class="product product-no-border style-2">
                                            <div class="product-container">
                                                <figure>
                                                    <div class="product-wrap">
                                                        <div class="product-images">
                                                            <div class="shop-loop-thumbnail shop-loop-front-thumbnail">
                                                                <a href="shop-detail-1.html"><img width="450" height="450" src="{{ asset('client/images/products/product_263x263.jpg') }}" alt=""/></a>
                                                            </div>
                                                            <div class="shop-loop-thumbnail shop-loop-back-thumbnail">
                                                                <a href="shop-detail-1.html"><img width="450" height="450" src="{{ asset('client/images/products/product_263x263alt.jpg') }}" alt=""/></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <figcaption>
                                                        <div class="shop-loop-product-info">
                                                            <div class="info-meta clearfix">
                                                                <div class="star-rating">
                                                                    <span style="width:0%"></span>
                                                                </div>
                                                                <div class="loop-add-to-wishlist">
                                                                    <div class="yith-wcwl-add-to-wishlist">
                                                                        <div class="yith-wcwl-add-button">
                                                                            <a href="#" class="add_to_wishlist">
                                                                                Add to Wishlist
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="info-content-wrap">
                                                                <h3 class="product_title">
                                                                    <a href="shop-detail-1.html">Saarinen Womb Chair</a>
                                                                </h3>
                                                                <div class="info-price">
																			<span class="price">
																				<span class="amount">&pound;123.00</span>
																			</span>
                                                                </div>
                                                                <div class="loop-action">
                                                                    <div class="loop-add-to-cart">
                                                                        <a href="#" class="add_to_cart_button">
                                                                            Add to cart
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </figcaption>
                                                </figure>
                                            </div>
                                        </li>
                                        <li class="product product-no-border style-2">
                                            <div class="product-container">
                                                <figure>
                                                    <div class="product-wrap">
                                                        <div class="product-images">
                                                            <div class="shop-loop-thumbnail shop-loop-front-thumbnail">
                                                                <a href="shop-detail-1.html"><img width="450" height="450" src="{{ asset('client/images/products/product_263x263.jpg') }}" alt=""/></a>
                                                            </div>
                                                            <div class="shop-loop-thumbnail shop-loop-back-thumbnail">
                                                                <a href="shop-detail-1.html"><img width="450" height="450" src="{{ asset('client/images/products/product_263x263alt.jpg') }}" alt=""/></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <figcaption>
                                                        <div class="shop-loop-product-info">
                                                            <div class="info-meta clearfix">
                                                                <div class="star-rating">
                                                                    <span style="width:0%"></span>
                                                                </div>
                                                                <div class="loop-add-to-wishlist">
                                                                    <div class="yith-wcwl-add-to-wishlist">
                                                                        <div class="yith-wcwl-add-button">
                                                                            <a href="#" class="add_to_wishlist">
                                                                                Add to Wishlist
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="info-content-wrap">
                                                                <h3 class="product_title">
                                                                    <a href="shop-detail-1.html">Jaime Hayon Ro Chair</a>
                                                                </h3>
                                                                <div class="info-price">
																			<span class="price">
																				<span class="amount">&pound;32.00</span>
																			</span>
                                                                </div>
                                                                <div class="loop-action">
                                                                    <div class="loop-add-to-cart">
                                                                        <a href="#" class="add_to_cart_button">
                                                                            Add to cart
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </figcaption>
                                                </figure>
                                            </div>
                                        </li>
                                        <li class="product product-no-border style-2">
                                            <div class="product-container">
                                                <figure>
                                                    <div class="product-wrap">
                                                        <div class="product-images">
                                                            <div class="shop-loop-thumbnail shop-loop-front-thumbnail">
                                                                <a href="shop-detail-1.html"><img width="450" height="450" src="{{ asset('client/images/products/product_263x263.jpg') }}" alt=""/></a>
                                                            </div>
                                                            <div class="shop-loop-thumbnail shop-loop-back-thumbnail">
                                                                <a href="shop-detail-1.html"><img width="450" height="450" src="{{ asset('client/images/products/product_263x263alt.jpg') }}" alt=""/></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <figcaption>
                                                        <div class="shop-loop-product-info">
                                                            <div class="info-meta clearfix">
                                                                <div class="star-rating">
                                                                    <span style="width:0%"></span>
                                                                </div>
                                                                <div class="loop-add-to-wishlist">
                                                                    <div class="yith-wcwl-add-to-wishlist">
                                                                        <div class="yith-wcwl-add-button">
                                                                            <a href="#" class="add_to_wishlist">
                                                                                Add to Wishlist
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="info-content-wrap">
                                                                <h3 class="product_title">
                                                                    <a href="shop-detail-1.html">Hans Wegner Shell Chair</a>
                                                                </h3>
                                                                <div class="info-price">
																			<span class="price">
																				<span class="amount">&pound;10.75</span>
																			</span>
                                                                </div>
                                                                <div class="loop-action">
                                                                    <div class="loop-add-to-cart">
                                                                        <a href="#" class="add_to_cart_button">
                                                                            Add to cart
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </figcaption>
                                                </figure>
                                            </div>
                                        </li>
                                        <li class="product product-no-border style-2">
                                            <div class="product-container">
                                                <figure>
                                                    <div class="product-wrap">
                                                        <div class="product-images">
                                                            <div class="shop-loop-thumbnail shop-loop-front-thumbnail">
                                                                <a href="shop-detail-1.html"><img width="450" height="450" src="{{ asset('client/images/products/product_263x263.jpg') }}" alt=""/></a>
                                                            </div>
                                                            <div class="shop-loop-thumbnail shop-loop-back-thumbnail">
                                                                <a href="shop-detail-1.html"><img width="450" height="450" src="{{ asset('client/images/products/product_263x263alt.jpg') }}" alt=""/></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <figcaption>
                                                        <div class="shop-loop-product-info">
                                                            <div class="info-meta clearfix">
                                                                <div class="star-rating">
                                                                    <span style="width:0%"></span>
                                                                </div>
                                                                <div class="loop-add-to-wishlist">
                                                                    <div class="yith-wcwl-add-to-wishlist">
                                                                        <div class="yith-wcwl-add-button">
                                                                            <a href="#" class="add_to_wishlist">
                                                                                Add to Wishlist
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="info-content-wrap">
                                                                <h3 class="product_title">
                                                                    <a href="shop-detail-1.html">Jens Risom Lounge</a>
                                                                </h3>
                                                                <div class="info-price">
																			<span class="price">
																				<span class="amount">&pound;17.45</span>
																			</span>
                                                                </div>
                                                                <div class="loop-action">
                                                                    <div class="loop-add-to-cart">
                                                                        <a href="#" class="add_to_cart_button">
                                                                            Add to cart
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </figcaption>
                                                </figure>
                                            </div>
                                        </li>
                                        <li class="product product-no-border style-2">
                                            <div class="product-container">
                                                <figure>
                                                    <div class="product-wrap">
                                                        <div class="product-images">
                                                            <div class="shop-loop-thumbnail shop-loop-front-thumbnail">
                                                                <a href="shop-detail-1.html"><img width="450" height="450" src="{{ asset('client/images/products/product_263x263.jpg') }}" alt=""/></a>
                                                            </div>
                                                            <div class="shop-loop-thumbnail shop-loop-back-thumbnail">
                                                                <a href="shop-detail-1.html"><img width="450" height="450" src="{{ asset('client/images/products/product_263x263alt.jpg') }}" alt=""/></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <figcaption>
                                                        <div class="shop-loop-product-info">
                                                            <div class="info-meta clearfix">
                                                                <div class="star-rating">
                                                                    <span style="width:0%"></span>
                                                                </div>
                                                                <div class="loop-add-to-wishlist">
                                                                    <div class="yith-wcwl-add-to-wishlist">
                                                                        <div class="yith-wcwl-add-button">
                                                                            <a href="#" class="add_to_wishlist">
                                                                                Add to Wishlist
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="info-content-wrap">
                                                                <h3 class="product_title">
                                                                    <a href="shop-detail-1.html">Eero Saarinen Oval Dining</a>
                                                                </h3>
                                                                <div class="info-price">
																			<span class="price">
																				<span class="amount">&pound;15.05</span>
																			</span>
                                                                </div>
                                                                <div class="loop-action">
                                                                    <div class="loop-add-to-cart">
                                                                        <a href="#" class="add_to_cart_button">
                                                                            Add to cart
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </figcaption>
                                                </figure>
                                            </div>
                                        </li>
                                        <li class="product product-no-border style-2">
                                            <div class="product-container">
                                                <figure>
                                                    <div class="product-wrap">
                                                        <div class="product-images">
                                                            <div class="shop-loop-thumbnail shop-loop-front-thumbnail">
                                                                <a href="shop-detail-1.html"><img width="450" height="450" src="{{ asset('client/images/products/product_263x263.jpg') }}" alt=""/></a>
                                                            </div>
                                                            <div class="shop-loop-thumbnail shop-loop-back-thumbnail">
                                                                <a href="shop-detail-1.html"><img width="450" height="450" src="{{ asset('client/images/products/product_263x263alt.jpg') }}" alt=""/></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <figcaption>
                                                        <div class="shop-loop-product-info">
                                                            <div class="info-meta clearfix">
                                                                <div class="star-rating">
                                                                    <span style="width:0%"></span>
                                                                </div>
                                                                <div class="loop-add-to-wishlist">
                                                                    <div class="yith-wcwl-add-to-wishlist">
                                                                        <div class="yith-wcwl-add-button">
                                                                            <a href="#" class="add_to_wishlist">
                                                                                Add to Wishlist
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="info-content-wrap">
                                                                <h3 class="product_title">
                                                                    <a href="shop-detail-1.html">Warren Platner Dining</a>
                                                                </h3>
                                                                <div class="info-price">
																			<span class="price">
																				<span class="amount">&pound;10.95</span>
																			</span>
                                                                </div>
                                                                <div class="loop-action">
                                                                    <div class="loop-add-to-cart">
                                                                        <a href="#" class="add_to_cart_button">
                                                                            Add to cart
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </figcaption>
                                                </figure>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <a href="#" class="caroufredsel-prev"></a>
                                <a href="#" class="caroufredsel-next"></a>
                            </div>
                        </div>
                        <div class="row row-fluid collection-header">
                            <a class="btn btn-black btn-align-center custom-1" href="#">
                                <span>SHOP CHAIRS</span>
                            </a>
                        </div>
                        <div class="row row-fluid collection">
                            <div class="row row-fluid">
                                <div class="col-sm-2"></div>
                                <div class="col-sm-8">
                                    <h2 class="text-center">TABLES</h2>
                                    <div class="separator separator_align_center sep_width_10 sep_border_width_2 sep_pos_align_center separator_no_text sep_color_black mb-4">
												<span class="sep_holder sep_holder_l">
													<span class="sep_line"></span>
												</span>
                                        <span class="sep_holder sep_holder_r">
													<span class="sep_line"></span>
												</span>
                                    </div>
                                    <p class="text-center">
                                        The Maecenas Collection does its job from a foundation of strength and function.Cras eu venenatis enim, sed facilisis leo. Sed lobortis egestas semper. Ut dapibus lacus sapien, nec tristique justo consequat dapibus. Aenean bibendum.
                                    </p>
                                </div>
                                <div class="col-sm-2"></div>
                            </div>
                            <div class="caroufredsel product-slider" data-height="variable" data-visible-min="1" data-scroll-item="" data-responsive="1" data-infinite="1" data-autoplay="0">
                                <div class="caroufredsel-wrap">
                                    <div class="commerce columns-4">
                                        <ul class="products columns-4" data-columns="4">
                                            <li class="product product-no-border style-2">
                                                <div class="product-container">
                                                    <figure>
                                                        <div class="product-wrap">
                                                            <div class="product-images">
                                                                <div class="shop-loop-thumbnail shop-loop-front-thumbnail">
                                                                    <a href="shop-detail-1.html"><img width="450" height="450" src="{{ asset('client/images/products/product_263x263.jpg') }}" alt=""/></a>
                                                                </div>
                                                                <div class="shop-loop-thumbnail shop-loop-back-thumbnail">
                                                                    <a href="shop-detail-1.html"><img width="450" height="450" src="{{ asset('client/images/products/product_263x263alt.jpg') }}" alt=""/></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <figcaption>
                                                            <div class="shop-loop-product-info">
                                                                <div class="info-meta clearfix">
                                                                    <div class="star-rating">
                                                                        <span style="width:0%"></span>
                                                                    </div>
                                                                    <div class="loop-add-to-wishlist">
                                                                        <div class="yith-wcwl-add-to-wishlist">
                                                                            <div class="yith-wcwl-add-button">
                                                                                <a href="#" class="add_to_wishlist">
                                                                                    Add to Wishlist
                                                                                </a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="info-content-wrap">
                                                                    <h3 class="product_title">
                                                                        <a href="shop-detail-1.html">Schultz Petal Dining</a>
                                                                    </h3>
                                                                    <div class="info-price">
																				<span class="price">
																					<span class="amount">&pound;17.45</span>
																				</span>
                                                                    </div>
                                                                    <div class="loop-action">
                                                                        <div class="loop-add-to-cart">
                                                                            <a href="#" class="add_to_cart_button">
                                                                                Add to cart
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </figcaption>
                                                    </figure>
                                                </div>
                                            </li>
                                            <li class="product product-no-border style-2">
                                                <div class="product-container">
                                                    <figure>
                                                        <div class="product-wrap">
                                                            <div class="product-images">
                                                                <div class="shop-loop-thumbnail shop-loop-front-thumbnail">
                                                                    <a href="shop-detail-1.html"><img width="450" height="450" src="{{ asset('client/images/products/product_263x263.jpg') }}" alt=""/></a>
                                                                </div>
                                                                <div class="shop-loop-thumbnail shop-loop-back-thumbnail">
                                                                    <a href="shop-detail-1.html"><img width="450" height="450" src="{{ asset('client/images/products/product_263x263alt.jpg') }}" alt=""/></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <figcaption>
                                                            <div class="shop-loop-product-info">
                                                                <div class="info-meta clearfix">
                                                                    <div class="star-rating">
                                                                        <span style="width:0%"></span>
                                                                    </div>
                                                                    <div class="loop-add-to-wishlist">
                                                                        <div class="yith-wcwl-add-to-wishlist">
                                                                            <div class="yith-wcwl-add-button">
                                                                                <a href="#" class="add_to_wishlist">
                                                                                    Add to Wishlist
                                                                                </a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="info-content-wrap">
                                                                    <h3 class="product_title">
                                                                        <a href="shop-detail-1.html">Jens Risom Lounge</a>
                                                                    </h3>
                                                                    <div class="info-price">
																				<span class="price">
																					<span class="amount">&pound;17.45</span>
																				</span>
                                                                    </div>
                                                                    <div class="loop-action">
                                                                        <div class="loop-add-to-cart">
                                                                            <a href="#" class="add_to_cart_button">
                                                                                Add to cart
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </figcaption>
                                                    </figure>
                                                </div>
                                            </li>
                                            <li class="product product-no-border style-2">
                                                <div class="product-container">
                                                    <figure>
                                                        <div class="product-wrap">
                                                            <div class="product-images">
                                                                <div class="shop-loop-thumbnail shop-loop-front-thumbnail">
                                                                    <a href="shop-detail-1.html"><img width="450" height="450" src="{{ asset('client/images/products/product_263x263.jpg') }}" alt=""/></a>
                                                                </div>
                                                                <div class="shop-loop-thumbnail shop-loop-back-thumbnail">
                                                                    <a href="shop-detail-1.html"><img width="450" height="450" src="{{ asset('client/images/products/product_263x263alt.jpg') }}" alt=""/></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <figcaption>
                                                            <div class="shop-loop-product-info">
                                                                <div class="info-meta clearfix">
                                                                    <div class="star-rating">
                                                                        <span style="width:0%"></span>
                                                                    </div>
                                                                    <div class="loop-add-to-wishlist">
                                                                        <div class="yith-wcwl-add-to-wishlist">
                                                                            <div class="yith-wcwl-add-button">
                                                                                <a href="#" class="add_to_wishlist">
                                                                                    Add to Wishlist
                                                                                </a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="info-content-wrap">
                                                                    <h3 class="product_title">
                                                                        <a href="shop-detail-1.html">Hans Wegner Shell Chair</a>
                                                                    </h3>
                                                                    <div class="info-price">
																				<span class="price">
																					<span class="amount">&pound;10.75</span>
																				</span>
                                                                    </div>
                                                                    <div class="loop-action">
                                                                        <div class="loop-add-to-cart">
                                                                            <a href="#" class="add_to_cart_button">
                                                                                Add to cart
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </figcaption>
                                                    </figure>
                                                </div>
                                            </li>
                                            <li class="product product-no-border style-2">
                                                <div class="product-container">
                                                    <figure>
                                                        <div class="product-wrap">
                                                            <div class="product-images">
                                                                <div class="shop-loop-thumbnail shop-loop-front-thumbnail">
                                                                    <a href="shop-detail-1.html"><img width="450" height="450" src="{{ asset('client/images/products/product_263x263.jpg') }}" alt=""/></a>
                                                                </div>
                                                                <div class="shop-loop-thumbnail shop-loop-back-thumbnail">
                                                                    <a href="shop-detail-1.html"><img width="450" height="450" src="{{ asset('client/images/products/product_263x263alt.jpg') }}" alt=""/></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <figcaption>
                                                            <div class="shop-loop-product-info">
                                                                <div class="info-meta clearfix">
                                                                    <div class="star-rating">
                                                                        <span style="width:0%"></span>
                                                                    </div>
                                                                    <div class="loop-add-to-wishlist">
                                                                        <div class="yith-wcwl-add-to-wishlist">
                                                                            <div class="yith-wcwl-add-button">
                                                                                <a href="#" class="add_to_wishlist">
                                                                                    Add to Wishlist
                                                                                </a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="info-content-wrap">
                                                                    <h3 class="product_title">
                                                                        <a href="shop-detail-1.html">Jaime Hayon Ro Chair</a>
                                                                    </h3>
                                                                    <div class="info-price">
																				<span class="price">
																					<span class="amount">&pound;32.00</span>
																				</span>
                                                                    </div>
                                                                    <div class="loop-action">
                                                                        <div class="loop-add-to-cart">
                                                                            <a href="#" class="add_to_cart_button">
                                                                                Add to cart
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </figcaption>
                                                    </figure>
                                                </div>
                                            </li>
                                            <li class="product product-no-border style-2">
                                                <div class="product-container">
                                                    <figure>
                                                        <div class="product-wrap">
                                                            <div class="product-images">
                                                                <div class="shop-loop-thumbnail shop-loop-front-thumbnail">
                                                                    <a href="shop-detail-1.html"><img width="450" height="450" src="{{ asset('client/images/products/product_263x263.jpg') }}" alt=""/></a>
                                                                </div>
                                                                <div class="shop-loop-thumbnail shop-loop-back-thumbnail">
                                                                    <a href="shop-detail-1.html"><img width="450" height="450" src="{{ asset('client/images/products/product_263x263alt.jpg') }}" alt=""/></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <figcaption>
                                                            <div class="shop-loop-product-info">
                                                                <div class="info-meta clearfix">
                                                                    <div class="star-rating">
                                                                        <span style="width:0%"></span>
                                                                    </div>
                                                                    <div class="loop-add-to-wishlist">
                                                                        <div class="yith-wcwl-add-to-wishlist">
                                                                            <div class="yith-wcwl-add-button">
                                                                                <a href="#" class="add_to_wishlist">
                                                                                    Add to Wishlist
                                                                                </a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="info-content-wrap">
                                                                    <h3 class="product_title">
                                                                        <a href="shop-detail-1.html">Saarinen Womb Chair</a>
                                                                    </h3>
                                                                    <div class="info-price">
																				<span class="price">
																					<span class="amount">&pound;123.00</span>
																				</span>
                                                                    </div>
                                                                    <div class="loop-action">
                                                                        <div class="loop-add-to-cart">
                                                                            <a href="#" class="add_to_cart_button">
                                                                                Add to cart
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </figcaption>
                                                    </figure>
                                                </div>
                                            </li>
                                            <li class="product product-no-border style-2">
                                                <div class="product-container">
                                                    <figure>
                                                        <div class="product-wrap">
                                                            <div class="product-images">
                                                                <div class="shop-loop-thumbnail shop-loop-front-thumbnail">
                                                                    <a href="shop-detail-1.html"><img width="450" height="450" src="{{ asset('client/images/products/product_263x263.jpg') }}" alt=""/></a>
                                                                </div>
                                                                <div class="shop-loop-thumbnail shop-loop-back-thumbnail">
                                                                    <a href="shop-detail-1.html"><img width="450" height="450" src="{{ asset('client/images/products/product_263x263alt.jpg') }}" alt=""/></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <figcaption>
                                                            <div class="shop-loop-product-info">
                                                                <div class="info-meta clearfix">
                                                                    <div class="star-rating">
                                                                        <span style="width:80%"></span>
                                                                    </div>
                                                                    <div class="loop-add-to-wishlist">
                                                                        <div class="yith-wcwl-add-to-wishlist">
                                                                            <div class="yith-wcwl-add-button">
                                                                                <a href="#" class="add_to_wishlist">
                                                                                    Add to Wishlist
                                                                                </a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="info-content-wrap">
                                                                    <h3 class="product_title">
                                                                        <a href="shop-detail-1.html">Citterio Grand Repos</a>
                                                                    </h3>
                                                                    <div class="info-price">
																				<span class="price">
																					<span class="amount">£12.00</span>
																					–
																					<span class="amount">£20.00</span>
																				</span>
                                                                    </div>
                                                                    <div class="loop-action">
                                                                        <div class="loop-add-to-cart">
                                                                            <a href="#" class="add_to_cart_button">
                                                                                Add to cart
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </figcaption>
                                                    </figure>
                                                </div>
                                            </li>
                                            <li class="product product-no-border style-2">
                                                <div class="product-container">
                                                    <figure>
                                                        <div class="product-wrap">
                                                            <div class="product-images">
                                                                <span class="onsale">Sale!</span>
                                                                <div class="shop-loop-thumbnail shop-loop-front-thumbnail">
                                                                    <a href="shop-detail-1.html"><img width="450" height="450" src="{{ asset('client/images/products/product_263x263.jpg') }}" alt=""/></a>
                                                                </div>
                                                                <div class="shop-loop-thumbnail shop-loop-back-thumbnail">
                                                                    <a href="shop-detail-1.html"><img width="450" height="450" src="{{ asset('client/images/products/product_263x263alt.jpg') }}" alt=""/></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <figcaption>
                                                            <div class="shop-loop-product-info">
                                                                <div class="info-meta clearfix">
                                                                    <div class="star-rating">
                                                                        <span style="width:80%"></span>
                                                                    </div>
                                                                    <div class="loop-add-to-wishlist">
                                                                        <div class="yith-wcwl-add-to-wishlist">
                                                                            <div class="yith-wcwl-add-button">
                                                                                <a href="#" class="add_to_wishlist">
                                                                                    Add to Wishlist
                                                                                </a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="info-content-wrap">
                                                                    <h3 class="product_title">
                                                                        <a href="shop-detail-1.html">Arne Jacobsen Oxford Chair</a>
                                                                    </h3>
                                                                    <div class="info-price">
																				<span class="price">
																					<del><span class="amount">£20.50</span></del>
																					<ins><span class="amount">£19.00</span></ins>
																				</span>
                                                                    </div>
                                                                    <div class="loop-action">
                                                                        <div class="loop-add-to-cart">
                                                                            <a href="#" class="add_to_cart_button">
                                                                                Add to cart
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </figcaption>
                                                    </figure>
                                                </div>
                                            </li>
                                            <li class="product product-no-border style-2">
                                                <div class="product-container">
                                                    <figure>
                                                        <div class="product-wrap">
                                                            <div class="product-images">
                                                                <span class="onsale">Sale!</span>
                                                                <div class="shop-loop-thumbnail shop-loop-front-thumbnail">
                                                                    <a href="shop-detail-1.html"><img width="450" height="450" src="{{ asset('client/images/products/product_263x263.jpg') }}" alt=""/></a>
                                                                </div>
                                                                <div class="shop-loop-thumbnail shop-loop-back-thumbnail">
                                                                    <a href="shop-detail-1.html"><img width="450" height="450" src="{{ asset('client/images/products/product_263x263alt.jpg') }}" alt=""/></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <figcaption>
                                                            <div class="shop-loop-product-info">
                                                                <div class="info-meta clearfix">
                                                                    <div class="star-rating">
                                                                        <span style="width:80%"></span>
                                                                    </div>
                                                                    <div class="loop-add-to-wishlist">
                                                                        <div class="yith-wcwl-add-to-wishlist">
                                                                            <div class="yith-wcwl-add-button">
                                                                                <a href="#" class="add_to_wishlist">
                                                                                    Add to Wishlist
                                                                                </a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="info-content-wrap">
                                                                    <h3 class="product_title">
                                                                        <a href="shop-detail-1.html">Charles Pollock Executive</a>
                                                                    </h3>
                                                                    <div class="info-price">
																				<span class="price">
																					<del><span class="amount">£20.50</span></del>
																					<ins><span class="amount">£19.00</span></ins>
																				</span>
                                                                    </div>
                                                                    <div class="loop-action">
                                                                        <div class="loop-add-to-cart">
                                                                            <a href="#" class="add_to_cart_button">
                                                                                Add to cart
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </figcaption>
                                                    </figure>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <a href="#" class="caroufredsel-prev"></a>
                                    <a href="#" class="caroufredsel-next"></a>
                                </div>
                            </div>
                            <div class="row row-fluid collection-header">
                                <a class="btn btn-black btn-align-center custom-1" href="#">
                                    <span>SHOP TABLES</span>
                                </a>
                            </div>
                        </div>
                        <div class="row row-fluid collection">
                            <div class="row row-fluid">
                                <div class="col-sm-2"></div>
                                <div class="col-sm-8">
                                    <h2 class="text-center">SHELVES</h2>
                                    <div class="separator separator_align_center sep_width_10 sep_border_width_2 sep_pos_align_center separator_no_text sep_color_black mb-4">
												<span class="sep_holder sep_holder_l">
													<span class="sep_line"></span>
												</span>
                                        <span class="sep_holder sep_holder_r">
													<span class="sep_line"></span>
												</span>
                                    </div>
                                    <p class="text-center">
                                        The Nulla Collection does its job from a foundation of strength and function.Cras eu venenatis enim, sed facilisis leo. Sed lobortis egestas semper. Ut dapibus lacus sapien, nec tristique justo consequat dapibus. Aenean bibendum.
                                    </p>
                                </div>
                                <div class="col-sm-2"></div>
                            </div>
                            <div class="caroufredsel product-slider" data-height="variable" data-visible-min="1" data-responsive="1" data-infinite="1" data-autoplay="0">
                                <div class="caroufredsel-wrap">
                                    <div class="commerce columns-4">
                                        <ul class="products columns-4" data-columns="4">
                                            <li class="product product-no-border style-2">
                                                <div class="product-container">
                                                    <figure>
                                                        <div class="product-wrap">
                                                            <div class="product-images">
                                                                <div class="shop-loop-thumbnail shop-loop-front-thumbnail">
                                                                    <a href="shop-detail-1.html"><img width="450" height="450" src="{{ asset('client/images/products/product_263x263.jpg') }}" alt=""/></a>
                                                                </div>
                                                                <div class="shop-loop-thumbnail shop-loop-back-thumbnail">
                                                                    <a href="shop-detail-1.html"><img width="450" height="450" src="{{ asset('client/images/products/product_263x263alt.jpg') }}" alt=""/></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <figcaption>
                                                            <div class="shop-loop-product-info">
                                                                <div class="info-meta clearfix">
                                                                    <div class="star-rating">
                                                                        <span style="width:0%"></span>
                                                                    </div>
                                                                    <div class="loop-add-to-wishlist">
                                                                        <div class="yith-wcwl-add-to-wishlist">
                                                                            <div class="yith-wcwl-add-button">
                                                                                <a href="#" class="add_to_wishlist">
                                                                                    Add to Wishlist
                                                                                </a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="info-content-wrap">
                                                                    <h3 class="product_title">
                                                                        <a href="shop-detail-1.html">Ainsley Etagere</a>
                                                                    </h3>
                                                                    <div class="info-price">
																				<span class="price">
																					<span class="amount">£10.95</span>
																				</span>
                                                                    </div>
                                                                    <div class="loop-action">
                                                                        <div class="loop-add-to-cart">
                                                                            <a href="#" class="add_to_cart_button">
                                                                                Add to cart
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </figcaption>
                                                    </figure>
                                                </div>
                                            </li>
                                            <li class="product product-no-border style-2">
                                                <div class="product-container">
                                                    <figure>
                                                        <div class="product-wrap">
                                                            <div class="product-images">
                                                                <span class="onsale">Sale!</span>
                                                                <div class="shop-loop-thumbnail shop-loop-front-thumbnail">
                                                                    <a href="shop-detail-1.html"><img width="450" height="450" src="{{ asset('client/images/products/product_263x263.jpg') }}" alt=""/></a>
                                                                </div>
                                                                <div class="shop-loop-thumbnail shop-loop-back-thumbnail">
                                                                    <a href="shop-detail-1.html"><img width="450" height="450" src="{{ asset('client/images/products/product_263x263alt.jpg') }}" alt=""/></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <figcaption>
                                                            <div class="shop-loop-product-info">
                                                                <div class="info-meta clearfix">
                                                                    <div class="star-rating">
                                                                        <span style="width:0%"></span>
                                                                    </div>
                                                                    <div class="loop-add-to-wishlist">
                                                                        <div class="yith-wcwl-add-to-wishlist">
                                                                            <div class="yith-wcwl-add-button">
                                                                                <a href="#" class="add_to_wishlist">
                                                                                    Add to Wishlist
                                                                                </a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="info-content-wrap">
                                                                    <h3 class="product_title">
                                                                        <a href="shop-detail-1.html">Arne Jacobsen Oxford Chair</a>
                                                                    </h3>
                                                                    <div class="info-price">
																				<span class="price">
																					<del><span class="amount">£20.50</span></del>
																					<ins><span class="amount">£19.00</span></ins>
																				</span>
                                                                    </div>
                                                                    <div class="loop-action">
                                                                        <div class="loop-add-to-cart">
                                                                            <a href="#" class="add_to_cart_button">
                                                                                Add to cart
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </figcaption>
                                                    </figure>
                                                </div>
                                            </li>
                                            <li class="product product-no-border style-2">
                                                <div class="product-container">
                                                    <figure>
                                                        <div class="product-wrap">
                                                            <div class="product-images">
                                                                <span class="onsale">Sale!</span>
                                                                <div class="shop-loop-thumbnail shop-loop-front-thumbnail">
                                                                    <a href="shop-detail-1.html"><img width="450" height="450" src="{{ asset('client/images/products/product_263x263.jpg') }}" alt=""/></a>
                                                                </div>
                                                                <div class="shop-loop-thumbnail shop-loop-back-thumbnail">
                                                                    <a href="shop-detail-1.html"><img width="450" height="450" src="{{ asset('client/images/products/product_263x263alt.jpg') }}" alt=""/></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <figcaption>
                                                            <div class="shop-loop-product-info">
                                                                <div class="info-meta clearfix">
                                                                    <div class="star-rating">
                                                                        <span style="width:80%"></span>
                                                                    </div>
                                                                    <div class="loop-add-to-wishlist">
                                                                        <div class="yith-wcwl-add-to-wishlist">
                                                                            <div class="yith-wcwl-add-button">
                                                                                <a href="#" class="add_to_wishlist">
                                                                                    Add to Wishlist
                                                                                </a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="info-content-wrap">
                                                                    <h3 class="product_title">
                                                                        <a href="shop-detail-1.html">Charles Pollock Executive</a>
                                                                    </h3>
                                                                    <div class="info-price">
																				<span class="price">
																					<del><span class="amount">£20.50</span></del>
																					<ins><span class="amount">£19.00</span></ins>
																				</span>
                                                                    </div>
                                                                    <div class="loop-action">
                                                                        <div class="loop-add-to-cart">
                                                                            <a href="#" class="add_to_cart_button">
                                                                                Add to cart
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </figcaption>
                                                    </figure>
                                                </div>
                                            </li>
                                            <li class="product product-no-border style-2">
                                                <div class="product-container">
                                                    <figure>
                                                        <div class="product-wrap">
                                                            <div class="product-images">
                                                                <div class="shop-loop-thumbnail shop-loop-front-thumbnail">
                                                                    <a href="shop-detail-1.html"><img width="450" height="450" src="{{ asset('client/images/products/product_263x263.jpg') }}" alt=""/></a>
                                                                </div>
                                                                <div class="shop-loop-thumbnail shop-loop-back-thumbnail">
                                                                    <a href="shop-detail-1.html"><img width="450" height="450" src="{{ asset('client/images/products/product_263x263alt.jpg') }}" alt=""/></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <figcaption>
                                                            <div class="shop-loop-product-info">
                                                                <div class="info-meta clearfix">
                                                                    <div class="star-rating">
                                                                        <span style="width:100%"></span>
                                                                    </div>
                                                                    <div class="loop-add-to-wishlist">
                                                                        <div class="yith-wcwl-add-to-wishlist">
                                                                            <div class="yith-wcwl-add-button">
                                                                                <a href="#" class="add_to_wishlist">
                                                                                    Add to Wishlist
                                                                                </a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="info-content-wrap">
                                                                    <h3 class="product_title">
                                                                        <a href="shop-detail-1.html">Citterio Grand Repos</a>
                                                                    </h3>
                                                                    <div class="info-price">
																				<span class="price">
																					<span class="amount">£12.00</span>
																					–
																					<span class="amount">£20.00</span>
																				</span>
                                                                    </div>
                                                                    <div class="loop-action">
                                                                        <div class="loop-add-to-cart">
                                                                            <a href="#" class="add_to_cart_button">
                                                                                Add to cart
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </figcaption>
                                                    </figure>
                                                </div>
                                            </li>
                                            <li class="product product-no-border style-2">
                                                <div class="product-container">
                                                    <figure>
                                                        <div class="product-wrap">
                                                            <div class="product-images">
                                                                <span class="onsale">Sale!</span>
                                                                <div class="shop-loop-thumbnail shop-loop-front-thumbnail">
                                                                    <a href="shop-detail-1.html"><img width="450" height="450" src="{{ asset('client/images/products/product_263x263.jpg') }}" alt=""/></a>
                                                                </div>
                                                                <div class="shop-loop-thumbnail shop-loop-back-thumbnail">
                                                                    <a href="shop-detail-1.html"><img width="450" height="450" src="{{ asset('client/images/products/product_263x263alt.jpg') }}" alt=""/></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <figcaption>
                                                            <div class="shop-loop-product-info">
                                                                <div class="info-meta clearfix">
                                                                    <div class="star-rating">
                                                                        <span style="width:0%"></span>
                                                                    </div>
                                                                    <div class="loop-add-to-wishlist">
                                                                        <div class="yith-wcwl-add-to-wishlist">
                                                                            <div class="yith-wcwl-add-button">
                                                                                <a href="#" class="add_to_wishlist">
                                                                                    Add to Wishlist
                                                                                </a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="info-content-wrap">
                                                                    <h3 class="product_title">
                                                                        <a href="shop-detail-1.html">Daniel Stromborg Round</a>
                                                                    </h3>
                                                                    <div class="info-price">
																				<span class="price">
																					<del><span class="amount">£23.00</span></del> <ins><span class="amount">£20.00</span></ins>
																				</span>
                                                                    </div>
                                                                    <div class="loop-action">
                                                                        <div class="loop-add-to-cart">
                                                                            <a href="#" class="add_to_cart_button">
                                                                                Add to cart
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </figcaption>
                                                    </figure>
                                                </div>
                                            </li>
                                            <li class="product product-no-border style-2">
                                                <div class="product-container">
                                                    <figure>
                                                        <div class="product-wrap">
                                                            <div class="product-images">
                                                                <div class="shop-loop-thumbnail shop-loop-front-thumbnail">
                                                                    <a href="shop-detail-1.html"><img width="450" height="450" src="{{ asset('client/images/products/product_263x263.jpg') }}" alt=""/></a>
                                                                </div>
                                                                <div class="shop-loop-thumbnail shop-loop-back-thumbnail">
                                                                    <a href="shop-detail-1.html"><img width="450" height="450" src="{{ asset('client/images/products/product_263x263alt.jpg') }}" alt=""/></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <figcaption>
                                                            <div class="shop-loop-product-info">
                                                                <div class="info-meta clearfix">
                                                                    <div class="star-rating">
                                                                        <span style="width:0%"></span>
                                                                    </div>
                                                                    <div class="loop-add-to-wishlist">
                                                                        <div class="yith-wcwl-add-to-wishlist">
                                                                            <div class="yith-wcwl-add-button">
                                                                                <a href="#" class="add_to_wishlist">
                                                                                    Add to Wishlist
                                                                                </a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="info-content-wrap">
                                                                    <h3 class="product_title">
                                                                        <a href="shop-detail-1.html">Eero Saarinen Oval Dining</a>
                                                                    </h3>
                                                                    <div class="info-price">
																				<span class="price">
																					<span class="amount">&pound;15.05</span>
																				</span>
                                                                    </div>
                                                                    <div class="loop-action">
                                                                        <div class="loop-add-to-cart">
                                                                            <a href="#" class="add_to_cart_button">
                                                                                Add to cart
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </figcaption>
                                                    </figure>
                                                </div>
                                            </li>
                                            <li class="product product-no-border style-2">
                                                <div class="product-container">
                                                    <figure>
                                                        <div class="product-wrap">
                                                            <div class="product-images">
                                                                <div class="shop-loop-thumbnail shop-loop-front-thumbnail">
                                                                    <a href="shop-detail-1.html"><img width="450" height="450" src="{{ asset('client/images/products/product_263x263.jpg') }}" alt=""/></a>
                                                                </div>
                                                                <div class="shop-loop-thumbnail shop-loop-back-thumbnail">
                                                                    <a href="shop-detail-1.html"><img width="450" height="450" src="{{ asset('client/images/products/product_263x263alt.jpg') }}" alt=""/></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <figcaption>
                                                            <div class="shop-loop-product-info">
                                                                <div class="info-meta clearfix">
                                                                    <div class="star-rating">
                                                                        <span style="width:0%"></span>
                                                                    </div>
                                                                    <div class="loop-add-to-wishlist">
                                                                        <div class="yith-wcwl-add-to-wishlist">
                                                                            <div class="yith-wcwl-add-button">
                                                                                <a href="#" class="add_to_wishlist">
                                                                                    Add to Wishlist
                                                                                </a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="info-content-wrap">
                                                                    <h3 class="product_title">
                                                                        <a href="shop-detail-1.html">Elv Console</a>
                                                                    </h3>
                                                                    <div class="info-price">
																				<span class="price">
																					<span class="amount">&pound;14.95</span>
																				</span>
                                                                    </div>
                                                                    <div class="loop-action">
                                                                        <div class="loop-add-to-cart">
                                                                            <a href="#" class="add_to_cart_button">
                                                                                Add to cart
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </figcaption>
                                                    </figure>
                                                </div>
                                            </li>
                                            <li class="product product-no-border style-2">
                                                <div class="product-container">
                                                    <figure>
                                                        <div class="product-wrap">
                                                            <div class="product-images">
                                                                <div class="shop-loop-thumbnail shop-loop-front-thumbnail">
                                                                    <a href="shop-detail-1.html"><img width="450" height="450" src="{{ asset('client/images/products/product_263x263.jpg') }}" alt=""/></a>
                                                                </div>
                                                                <div class="shop-loop-thumbnail shop-loop-back-thumbnail">
                                                                    <a href="shop-detail-1.html"><img width="450" height="450" src="{{ asset('client/images/products/product_263x263alt.jpg') }}" alt=""/></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <figcaption>
                                                            <div class="shop-loop-product-info">
                                                                <div class="info-meta clearfix">
                                                                    <div class="star-rating">
                                                                        <span style="width:100%"></span>
                                                                    </div>
                                                                    <div class="loop-add-to-wishlist">
                                                                        <div class="yith-wcwl-add-to-wishlist">
                                                                            <div class="yith-wcwl-add-button">
                                                                                <a href="#" class="add_to_wishlist">
                                                                                    Add to Wishlist
                                                                                </a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="info-content-wrap">
                                                                    <h3 class="product_title">
                                                                        <a href="shop-detail-1.html">Florence Knoll Credenza</a>
                                                                    </h3>
                                                                    <div class="info-price">
																				<span class="price">
																					<span class="amount">£17.50</span>
																				</span>
                                                                    </div>
                                                                    <div class="loop-action">
                                                                        <div class="loop-add-to-cart">
                                                                            <a href="#" class="add_to_cart_button">
                                                                                Add to cart
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </figcaption>
                                                    </figure>
                                                </div>
                                            </li>
                                            <li class="product product-no-border style-2">
                                                <div class="product-container">
                                                    <figure>
                                                        <div class="product-wrap">
                                                            <div class="product-images">
                                                                <div class="shop-loop-thumbnail shop-loop-front-thumbnail">
                                                                    <a href="shop-detail-1.html"><img width="450" height="450" src="{{ asset('client/images/products/product_263x263.jpg') }}" alt=""/></a>
                                                                </div>
                                                                <div class="shop-loop-thumbnail shop-loop-back-thumbnail">
                                                                    <a href="shop-detail-1.html"><img width="450" height="450" src="{{ asset('client/images/products/product_263x263alt.jpg') }}" alt=""/></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <figcaption>
                                                            <div class="shop-loop-product-info">
                                                                <div class="info-meta clearfix">
                                                                    <div class="star-rating">
                                                                        <span style="width:0%"></span>
                                                                    </div>
                                                                    <div class="loop-add-to-wishlist">
                                                                        <div class="yith-wcwl-add-to-wishlist">
                                                                            <div class="yith-wcwl-add-button">
                                                                                <a href="#" class="add_to_wishlist">
                                                                                    Add to Wishlist
                                                                                </a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="info-content-wrap">
                                                                    <h3 class="product_title">
                                                                        <a href="shop-detail-1.html">Florence Knoll Executive</a>
                                                                    </h3>
                                                                    <div class="info-price">
																				<span class="price">
																					<del><span class="amount">£20.50</span></del>
																					<ins><span class="amount">£19.00</span></ins>
																				</span>
                                                                    </div>
                                                                    <div class="loop-action">
                                                                        <div class="loop-add-to-cart">
                                                                            <a href="#" class="add_to_cart_button">
                                                                                Add to cart
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </figcaption>
                                                    </figure>
                                                </div>
                                            </li>
                                            <li class="product product-no-border style-2">
                                                <div class="product-container">
                                                    <figure>
                                                        <div class="product-wrap">
                                                            <div class="product-images">
                                                                <span class="onsale">Sale!</span>
                                                                <div class="shop-loop-thumbnail shop-loop-front-thumbnail">
                                                                    <a href="shop-detail-1.html"><img width="450" height="450" src="{{ asset('client/images/products/product_263x263.jpg') }}" alt=""/></a>
                                                                </div>
                                                                <div class="shop-loop-thumbnail shop-loop-back-thumbnail">
                                                                    <a href="shop-detail-1.html"><img width="450" height="450" src="{{ asset('client/images/products/product_263x263alt.jpg') }}" alt=""/></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <figcaption>
                                                            <div class="shop-loop-product-info">
                                                                <div class="info-meta clearfix">
                                                                    <div class="star-rating">
                                                                        <span style="width:0%"></span>
                                                                    </div>
                                                                    <div class="loop-add-to-wishlist">
                                                                        <div class="yith-wcwl-add-to-wishlist">
                                                                            <div class="yith-wcwl-add-button">
                                                                                <a href="#" class="add_to_wishlist">
                                                                                    Add to Wishlist
                                                                                </a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="info-content-wrap">
                                                                    <h3 class="product_title">
                                                                        <a href="shop-detail-1.html">Hans Wegner Desk</a>
                                                                    </h3>
                                                                    <div class="info-price">
																				<span class="price">
																					<del><span class="amount">£20.50</span></del>
																					<ins><span class="amount">£19.00</span></ins>
																				</span>
                                                                    </div>
                                                                    <div class="loop-action">
                                                                        <div class="loop-add-to-cart">
                                                                            <a href="#" class="add_to_cart_button">
                                                                                Add to cart
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </figcaption>
                                                    </figure>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <a href="#" class="caroufredsel-prev"></a>
                                    <a href="#" class="caroufredsel-next"></a>
                                </div>
                            </div>
                            <div class="row row-fluid collection-header">
                                <a class="btn btn-black btn-align-center custom-1">
                                    <span>SHOP SHELVES</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection