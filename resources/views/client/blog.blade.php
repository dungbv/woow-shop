@extends('layouts.app')
@section('title')
    Blog
@endsection
@section('content')
    @component('layouts.client-partial.breadcrumb_content')
        @slot('curent_page')
            Blog
        @endslot
    @endcomponent
    <div class="content-container">
        <div class="container">
            <div class="row">
                <div class="col-md-9 main-wrap">
                    <div class="main-content">
                        <div class="posts" data-layout="default">
                            <div class="posts-wrap posts-layout-default">
                                <article class="hentry">
                                    <div class="hentry-wrap">
                                        <div class="entry-featured">
                                            <a href="blog-detail.html">
                                                <img width="700" height="450" src="{{ asset('client/images/blog/blog_870x559.jpg') }}" alt="Blog-1"/>
                                            </a>
                                        </div>
                                        <div class="entry-info">
                                            <div class="entry-header">
                                                <h2 class="entry-title">
                                                    <a href="blog-detail.html">Monogrammed Speedy in Tow </a>
                                                </h2>
                                            </div>
                                            <div class="entry-content">
                                                The summer holidays are wonderful. Dressing for them can be significantly less so: Packing light is always at a premium, but one never wants to feel high, dry, and seriously...
                                            </div>
                                            <div class="entry-meta icon-meta">
														<span class="meta-date">
															Date:
															<time datetime="2015-08-11T06:27:49+00:00">
																August 11, 2015
															</time>
														</span>
                                                <span class="meta-author">
															By:
															<a href="#">sitesao</a>
														</span>
                                                <span class="meta-category">
															Category:
															<a href="#">Aliquam</a>, <a href="#">Nunc</a>
														</span>
                                            </div>
                                            <div class="readmore-link">
                                                <a href="blog-detail.html">Read More</a>
                                            </div>
                                        </div>
                                    </div>
                                </article>
                                <article class="hentry">
                                    <div class="hentry-wrap">
                                        <div class="entry-featured">
                                            <a href="blog-detail.html">
                                                <img width="700" height="450" src="{{ asset('client/images/blog/blog_870x559.jpg') }}" alt="Blog-2"/>
                                            </a>
                                        </div>
                                        <div class="entry-info">
                                            <div class="entry-header">
                                                <h2 class="entry-title">
                                                    <a href="blog-detail.html">Summer Classics in Positano </a>
                                                </h2>
                                            </div>
                                            <div class="entry-content">
                                                The summer holidays are wonderful. Dressing for them can be significantly less so: Packing light is always at a premium, but one never wants to feel high, dry, and seriously...
                                            </div>
                                            <div class="entry-meta icon-meta">
														<span class="meta-date">
															Date:
															<time datetime="2015-08-11T06:27:49+00:00">
																August 11, 2015
															</time>
														</span>
                                                <span class="meta-author">
															By:
															<a href="#">sitesao</a>
														</span>
                                                <span class="meta-category">
															Category:
															<a href="#">Aliquam</a>, <a href="#">Nunc</a>
														</span>
                                            </div>
                                            <div class="readmore-link">
                                                <a href="blog-detail.html">Read More</a>
                                            </div>
                                        </div>
                                    </div>
                                </article>
                                <article class="hentry">
                                    <div class="hentry-wrap">
                                        <div class="entry-featured">
                                            <a href="blog-detail.html">
                                                <img width="700" height="450" src="{{ asset('client/images/blog/blog_870x559.jpg') }}" alt="Blog-3"/>
                                            </a>
                                        </div>
                                        <div class="entry-info">
                                            <div class="entry-header">
                                                <h2 class="entry-title">
                                                    <a href="blog-detail.html">That Most Modern </a>
                                                </h2>
                                            </div>
                                            <div class="entry-content">
                                                The summer holidays are wonderful. Dressing for them can be significantly less so: Packing light is always at a premium, but one never wants to feel high, dry, and seriously...
                                            </div>
                                            <div class="entry-meta icon-meta">
														<span class="meta-date">
															Date:
															<time datetime="2015-08-11T06:27:49+00:00">
																August 11, 2015
															</time>
														</span>
                                                <span class="meta-author">
															By:
															<a href="#">sitesao</a>
														</span>
                                                <span class="meta-category">
															Category:
															<a href="#">Aliquam</a>, <a href="#">Nunc</a>
														</span>
                                            </div>
                                            <div class="readmore-link">
                                                <a href="blog-detail.html">Read More</a>
                                            </div>
                                        </div>
                                    </div>
                                </article>
                            </div>
                            <div class="paginate">
                                <div class="paginate_links">
                                    <span class='page-numbers current'>1</span>
                                    <a class='page-numbers' href='#'>2</a>
                                    <a class="next page-numbers" href="#">
                                        <i class="fa fa-angle-right"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 sidebar-wrap">
                    <div class="main-sidebar">
                        <div class="widget widget_categories">
                            <h4 class="widget-title"><span>Categories</span></h4>
                            <ul>
                                <li><a href="#">Aliquam</a> (6)</li>
                                <li><a href="#">Cras</a> (2)</li>
                                <li><a href="#">Nunc</a> (10)</li>
                                <li><a href="#">Praesent</a> (4)</li>
                            </ul>
                        </div>
                        <div class="widget widget-post-thumbnail">
                            <h4 class="widget-title"><span>latest post</span></h4>
                            <ul class="posts-thumbnail-list">
                                <li>
                                    <div class="posts-thumbnail-image">
                                        <a href="blog-detail.html">
                                            <img width="300" height="300" src="{{ asset('client/images/blog/thumb/blog_70x70.jpg') }}" alt="Blog-1"/>
                                        </a>
                                    </div>
                                    <div class="posts-thumbnail-content">
                                        <h4><a href="blog-detail.html">Monogrammed Speedy in Tow</a></h4>
                                        <div class="posts-thumbnail-meta">
                                            <time datetime="2015-08-11T06:27:49+00:00">August 11, 2015</time>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="posts-thumbnail-image">
                                        <a href="blog-detail.html">
                                            <img width="300" height="300" src="{{ asset('client/images/blog/thumb/blog_70x70.jpg') }}" alt="Blog-2"/>
                                        </a>
                                    </div>
                                    <div class="posts-thumbnail-content">
                                        <h4><a href="blog-detail.html">Summer Classics in Positano</a></h4>
                                        <div class="posts-thumbnail-meta">
                                            <time datetime="2015-08-11T06:27:49+00:00">August 11, 2015</time>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="posts-thumbnail-image">
                                        <a href="blog-detail.html">
                                            <img width="300" height="300" src="{{ asset('client/images/blog/thumb/blog_70x70.jpg') }}" alt="Blog-3"/>
                                        </a>
                                    </div>
                                    <div class="posts-thumbnail-content">
                                        <h4><a href="blog-detail.html">That Most Modern</a></h4>
                                        <div class="posts-thumbnail-meta">
                                            <time datetime="2015-08-11T06:27:49+00:00">August 11, 2015</time>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="posts-thumbnail-image">
                                        <a href="blog-detail.html">
                                            <img width="300" height="300" src="{{ asset('client/images/blog/thumb/blog_70x70.jpg') }}" alt="Blog-4"/>
                                        </a>
                                    </div>
                                    <div class="posts-thumbnail-content">
                                        <h4><a href="blog-detail.html">Giambattista Valli</a></h4>
                                        <div class="posts-thumbnail-meta">
                                            <time datetime="2015-08-11T06:27:49+00:00">August 11, 2015</time>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="widget widget_tag_cloud">
                            <h4 class="widget-title"><span>Tags</span></h4>
                            <div class="tagcloud">
                                <a href='#'>Deals</a>
                                <a href='#'>Dolor</a>
                                <a href='#'>Lacus</a>
                                <a href='#'>Praesent</a>
                                <a href='#'>Quam</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection