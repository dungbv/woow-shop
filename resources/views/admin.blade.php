@extends('layouts.app-admin')

@section('content')
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3>150</h3>

                        <p>New Orders</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-bag"></i>
                    </div>
                    <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3>53<sup style="font-size: 20px">%</sup></h3>

                        <p>Bounce Rate</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                    </div>
                    <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-yellow">
                    <div class="inner">
                        <h3>44</h3>

                        <p>User Registrations</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-person-add"></i>
                    </div>
                    <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-red">
                    <div class="inner">
                        <h3>65</h3>

                        <p>Unique Visitors</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-pie-graph"></i>
                    </div>
                    <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
        </div>
        <!-- /.row -->
        <!-- Main row -->
        <div class="row">
            <!-- Left col -->
            <section class="col-lg-7 connectedSortable ui-sortable">
                <!-- quick email widget -->
                <div class="box box-info">
                    <div class="box-header ui-sortable-handle" style="cursor: move;">
                        <i class="fa fa-envelope"></i>

                        <h3 class="box-title">Quick Email</h3>
                        <!-- tools box -->
                        <div class="pull-right box-tools">
                            <button type="button" class="btn btn-info btn-sm" data-widget="remove"
                                    data-toggle="tooltip" title="" data-original-title="Remove">
                                <i class="fa fa-times"></i></button>
                        </div>
                        <!-- /. tools -->
                    </div>
                    <div class="box-body">
                        <form action="#" method="post">
                            <div class="form-group">
                                <input type="email" class="form-control" name="emailto" placeholder="Email to:">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="subject" placeholder="Subject">
                            </div>
                            <div>
                                <ul class="wysihtml5-toolbar" style="">
                                    <li class="dropdown">
                                        <a class="btn btn-default dropdown-toggle " data-toggle="dropdown">

                                            <span class="glyphicon glyphicon-font"></span>

                                            <span class="current-font">Normal text</span>
                                            <b class="caret"></b>
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li><a data-wysihtml5-command="formatBlock"
                                                   data-wysihtml5-command-value="p" tabindex="-1"
                                                   href="javascript:;" unselectable="on">Normal text</a></li>
                                            <li><a data-wysihtml5-command="formatBlock"
                                                   data-wysihtml5-command-value="h1" tabindex="-1"
                                                   href="javascript:;" unselectable="on">Heading 1</a></li>
                                            <li><a data-wysihtml5-command="formatBlock"
                                                   data-wysihtml5-command-value="h2" tabindex="-1"
                                                   href="javascript:;" unselectable="on">Heading 2</a></li>
                                            <li><a data-wysihtml5-command="formatBlock"
                                                   data-wysihtml5-command-value="h3" tabindex="-1"
                                                   href="javascript:;" unselectable="on">Heading 3</a></li>
                                            <li><a data-wysihtml5-command="formatBlock"
                                                   data-wysihtml5-command-value="h4" tabindex="-1"
                                                   href="javascript:;" unselectable="on">Heading 4</a></li>
                                            <li><a data-wysihtml5-command="formatBlock"
                                                   data-wysihtml5-command-value="h5" tabindex="-1"
                                                   href="javascript:;" unselectable="on">Heading 5</a></li>
                                            <li><a data-wysihtml5-command="formatBlock"
                                                   data-wysihtml5-command-value="h6" tabindex="-1"
                                                   href="javascript:;" unselectable="on">Heading 6</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <div class="btn-group">
                                            <a class="btn  btn-default" data-wysihtml5-command="bold" title="CTRL+B"
                                               tabindex="-1" href="javascript:;" unselectable="on">Bold</a>
                                            <a class="btn  btn-default" data-wysihtml5-command="italic"
                                               title="CTRL+I" tabindex="-1" href="javascript:;"
                                               unselectable="on">Italic</a>
                                            <a class="btn  btn-default" data-wysihtml5-command="underline"
                                               title="CTRL+U" tabindex="-1" href="javascript:;" unselectable="on">Underline</a>

                                            <a class="btn  btn-default" data-wysihtml5-command="small"
                                               title="CTRL+S" tabindex="-1" href="javascript:;"
                                               unselectable="on">Small</a>

                                        </div>
                                    </li>
                                    <li>
                                        <a class="btn  btn-default" data-wysihtml5-command="formatBlock"
                                           data-wysihtml5-command-value="blockquote"
                                           data-wysihtml5-display-format-name="false" tabindex="-1"
                                           href="javascript:;" unselectable="on">

                                            <span class="glyphicon glyphicon-quote"></span>

                                        </a>
                                    </li>
                                    <li>
                                        <div class="btn-group">
                                            <a class="btn  btn-default" data-wysihtml5-command="insertUnorderedList"
                                               title="Unordered list" tabindex="-1" href="javascript:;"
                                               unselectable="on">

                                                <span class="glyphicon glyphicon-list"></span>

                                            </a>
                                            <a class="btn  btn-default" data-wysihtml5-command="insertOrderedList"
                                               title="Ordered list" tabindex="-1" href="javascript:;"
                                               unselectable="on">

                                                <span class="glyphicon glyphicon-th-list"></span>

                                            </a>
                                            <a class="btn  btn-default" data-wysihtml5-command="Outdent"
                                               title="Outdent" tabindex="-1" href="javascript:;" unselectable="on">

                                                <span class="glyphicon glyphicon-indent-right"></span>

                                            </a>
                                            <a class="btn  btn-default" data-wysihtml5-command="Indent"
                                               title="Indent" tabindex="-1" href="javascript:;" unselectable="on">

                                                <span class="glyphicon glyphicon-indent-left"></span>

                                            </a>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="bootstrap-wysihtml5-insert-link-modal modal fade"
                                             data-wysihtml5-dialog="createLink">
                                            <div class="modal-dialog ">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <a class="close" data-dismiss="modal">×</a>
                                                        <h3>Insert link</h3>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="form-group">
                                                            <input value="http://"
                                                                   class="bootstrap-wysihtml5-insert-link-url form-control"
                                                                   data-wysihtml5-dialog-field="href">
                                                        </div>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox"
                                                                       class="bootstrap-wysihtml5-insert-link-target"
                                                                       checked="">Open link in new window
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <a class="btn btn-default" data-dismiss="modal"
                                                           data-wysihtml5-dialog-action="cancel" href="#">Cancel</a>
                                                        <a href="#" class="btn btn-primary" data-dismiss="modal"
                                                           data-wysihtml5-dialog-action="save">Insert link</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <a class="btn  btn-default" data-wysihtml5-command="createLink"
                                           title="Insert link" tabindex="-1" href="javascript:;" unselectable="on">

                                            <span class="glyphicon glyphicon-share"></span>

                                        </a>
                                    </li>
                                    <li>
                                        <div class="bootstrap-wysihtml5-insert-image-modal modal fade"
                                             data-wysihtml5-dialog="insertImage">
                                            <div class="modal-dialog ">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <a class="close" data-dismiss="modal">×</a>
                                                        <h3>Insert image</h3>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="form-group">
                                                            <input value="http://"
                                                                   class="bootstrap-wysihtml5-insert-image-url form-control"
                                                                   data-wysihtml5-dialog-field="src">
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <a class="btn btn-default" data-dismiss="modal"
                                                           data-wysihtml5-dialog-action="cancel" href="#">Cancel</a>
                                                        <a class="btn btn-primary" data-dismiss="modal"
                                                           data-wysihtml5-dialog-action="save" href="#">Insert
                                                            image</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <a class="btn  btn-default" data-wysihtml5-command="insertImage"
                                           title="Insert image" tabindex="-1" href="javascript:;" unselectable="on">

                                            <span class="glyphicon glyphicon-picture"></span>

                                        </a>
                                    </li>
                                </ul>
                                <textarea class="textarea"
                                          style="width: 100%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid rgb(221, 221, 221); padding: 10px; display: none;"
                                          placeholder="Message"></textarea><input type="hidden"
                                                                                  name="_wysihtml5_mode" value="1">
                                <iframe class="wysihtml5-sandbox" security="restricted" allowtransparency="true"
                                        frameborder="0" width="0" height="0" marginwidth="0" marginheight="0"
                                        style="display: inline-block; background-color: rgb(255, 255, 255); border-collapse: separate; border-color: rgb(221, 221, 221); border-style: solid; border-width: 1px; clear: none; float: none; margin: 0px; outline: rgb(51, 51, 51) none 0px; outline-offset: 0px; padding: 10px; position: static; top: auto; left: auto; right: auto; bottom: auto; z-index: auto; vertical-align: baseline; text-align: start; box-sizing: border-box; box-shadow: none; border-radius: 0px; width: 100%; height: 125px;"></iframe>
                            </div>
                        </form>
                    </div>
                    <div class="box-footer clearfix">
                        <button type="button" class="pull-right btn btn-default" id="sendEmail">Send
                            <i class="fa fa-arrow-circle-right"></i></button>
                    </div>
                </div>

            </section>
            <!-- /.Left col -->
            <!-- right col (We are only adding the ID to make the widgets sortable)-->
            <section class="col-lg-5 connectedSortable ui-sortable">
                <!-- solid sales graph -->
                <div class="box box-solid bg-teal-gradient">
                    <div class="box-header ui-sortable-handle" style="cursor: move;">
                        <i class="fa fa-th"></i>

                        <h3 class="box-title">Sales Graph</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn bg-teal btn-sm" data-widget="collapse"><i
                                        class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn bg-teal btn-sm" data-widget="remove"><i
                                        class="fa fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="box-body border-radius-none">
                        <div class="chart" id="line-chart"
                             style="height: 250px; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                            <svg height="250" version="1.1" width="390.406" xmlns="http://www.w3.org/2000/svg"
                                 xmlns:xlink="http://www.w3.org/1999/xlink"
                                 style="overflow: hidden; position: relative; left: -0.578125px; top: -0.59375px;">
                                <desc style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">Created with Raphaël
                                    2.2.0
                                </desc>
                                <defs style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></defs>
                                <text x="41" y="214" text-anchor="end" font-family="Open Sans" font-size="10px"
                                      stroke="none" fill="#ffffff"
                                      style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-family: &quot;Open Sans&quot;; font-size: 10px; font-weight: normal;"
                                      font-weight="normal">
                                    <tspan dy="3.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">0</tspan>
                                </text>
                                <path fill="none" stroke="#efefef" d="M53.5,214H365.406" stroke-width="0.4"
                                      style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path>
                                <text x="41" y="166.75" text-anchor="end" font-family="Open Sans" font-size="10px"
                                      stroke="none" fill="#ffffff"
                                      style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-family: &quot;Open Sans&quot;; font-size: 10px; font-weight: normal;"
                                      font-weight="normal">
                                    <tspan dy="3.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">5,000
                                    </tspan>
                                </text>
                                <path fill="none" stroke="#efefef" d="M53.5,166.75H365.406" stroke-width="0.4"
                                      style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path>
                                <text x="41" y="119.5" text-anchor="end" font-family="Open Sans" font-size="10px"
                                      stroke="none" fill="#ffffff"
                                      style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-family: &quot;Open Sans&quot;; font-size: 10px; font-weight: normal;"
                                      font-weight="normal">
                                    <tspan dy="3.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">10,000
                                    </tspan>
                                </text>
                                <path fill="none" stroke="#efefef" d="M53.5,119.5H365.406" stroke-width="0.4"
                                      style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path>
                                <text x="41" y="72.25" text-anchor="end" font-family="Open Sans" font-size="10px"
                                      stroke="none" fill="#ffffff"
                                      style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-family: &quot;Open Sans&quot;; font-size: 10px; font-weight: normal;"
                                      font-weight="normal">
                                    <tspan dy="3.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">15,000
                                    </tspan>
                                </text>
                                <path fill="none" stroke="#efefef" d="M53.5,72.25H365.406" stroke-width="0.4"
                                      style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path>
                                <text x="41" y="25" text-anchor="end" font-family="Open Sans" font-size="10px"
                                      stroke="none" fill="#ffffff"
                                      style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-family: &quot;Open Sans&quot;; font-size: 10px; font-weight: normal;"
                                      font-weight="normal">
                                    <tspan dy="3.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">20,000
                                    </tspan>
                                </text>
                                <path fill="none" stroke="#efefef" d="M53.5,25H365.406" stroke-width="0.4"
                                      style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path>
                                <text x="308.1790182260024" y="226.5" text-anchor="middle" font-family="Open Sans"
                                      font-size="10px" stroke="none" fill="#ffffff"
                                      style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-family: &quot;Open Sans&quot;; font-size: 10px; font-weight: normal;"
                                      font-weight="normal" transform="matrix(1,0,0,1,0,5.5)">
                                    <tspan dy="3.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">2013
                                    </tspan>
                                </text>
                                <text x="169.4699100850547" y="226.5" text-anchor="middle" font-family="Open Sans"
                                      font-size="10px" stroke="none" fill="#ffffff"
                                      style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-family: &quot;Open Sans&quot;; font-size: 10px; font-weight: normal;"
                                      font-weight="normal" transform="matrix(1,0,0,1,0,5.5)">
                                    <tspan dy="3.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">2012
                                    </tspan>
                                </text>
                                <path fill="none" stroke="#efefef"
                                      d="M53.5,188.8063C62.21669258809234,188.5417,79.65007776427704,190.4009875,88.36677035236939,187.74790000000002C97.08346294046173,185.09481250000002,114.51684811664643,168.75624016393445,123.23354070473877,167.5816C131.8554866342649,166.41972766393442,149.09937849331715,180.6438625,157.72132442284328,178.40185C166.34327035236942,176.1598375,183.58716221142163,151.8811350409836,192.20910814094776,149.6455C200.9258007290401,147.3852975409836,218.35918590522482,158.0678125,227.07587849331716,160.4185C235.7925710814095,162.7691875,253.22595625759422,179.6189893442623,261.94264884568656,168.451C270.5645947752127,157.40440184426228,287.8084866342649,78.52883321823204,296.43043256379104,71.56015C304.95763183475094,64.66804571823204,322.01203037667074,105.24937403846154,330.53922964763063,113.00785C339.255922235723,120.93873653846154,356.68930741190763,128.9901625,365.406,134.3176"
                                      stroke-width="2"
                                      style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path>
                                <circle cx="53.5" cy="188.8063" r="4" fill="#efefef" stroke="#efefef"
                                        stroke-width="1"
                                        style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                                <circle cx="88.36677035236939" cy="187.74790000000002" r="4" fill="#efefef"
                                        stroke="#efefef" stroke-width="1"
                                        style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                                <circle cx="123.23354070473877" cy="167.5816" r="4" fill="#efefef" stroke="#efefef"
                                        stroke-width="1"
                                        style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                                <circle cx="157.72132442284328" cy="178.40185" r="4" fill="#efefef" stroke="#efefef"
                                        stroke-width="1"
                                        style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                                <circle cx="192.20910814094776" cy="149.6455" r="4" fill="#efefef" stroke="#efefef"
                                        stroke-width="1"
                                        style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                                <circle cx="227.07587849331716" cy="160.4185" r="4" fill="#efefef" stroke="#efefef"
                                        stroke-width="1"
                                        style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                                <circle cx="261.94264884568656" cy="168.451" r="4" fill="#efefef" stroke="#efefef"
                                        stroke-width="1"
                                        style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                                <circle cx="296.43043256379104" cy="71.56015" r="4" fill="#efefef" stroke="#efefef"
                                        stroke-width="1"
                                        style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                                <circle cx="330.53922964763063" cy="113.00785" r="4" fill="#efefef" stroke="#efefef"
                                        stroke-width="1"
                                        style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                                <circle cx="365.406" cy="134.3176" r="4" fill="#efefef" stroke="#efefef"
                                        stroke-width="1"
                                        style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                            </svg>
                            <div class="morris-hover morris-default-style"
                                 style="left: 10.4766px; top: 121px; display: none;">
                                <div class="morris-hover-row-label">2011 Q1</div>
                                <div class="morris-hover-point" style="color: #efefef">
                                    Item 1:
                                    2,666
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer no-border">
                        <div class="row">
                            <div class="col-xs-4 text-center" style="border-right: 1px solid #f4f4f4">
                                <div style="display:inline;width:60px;height:60px;">
                                    <canvas width="60" height="60"></canvas>
                                    <input type="text" class="knob" data-readonly="true" value="20" data-width="60"
                                           data-height="60" data-fgcolor="#39CCCC" readonly="readonly"
                                           style="width: 34px; height: 20px; position: absolute; vertical-align: middle; margin-top: 20px; margin-left: -47px; border: 0px; background: none; font-style: normal; font-variant: normal; font-weight: bold; font-stretch: normal; font-size: 12px; line-height: normal; font-family: Arial; text-align: center; color: rgb(57, 204, 204); padding: 0px; -webkit-appearance: none;">
                                </div>

                                <div class="knob-label">Mail-Orders</div>
                            </div>
                            <!-- ./col -->
                            <div class="col-xs-4 text-center" style="border-right: 1px solid #f4f4f4">
                                <div style="display:inline;width:60px;height:60px;">
                                    <canvas width="60" height="60"></canvas>
                                    <input type="text" class="knob" data-readonly="true" value="50" data-width="60"
                                           data-height="60" data-fgcolor="#39CCCC" readonly="readonly"
                                           style="width: 34px; height: 20px; position: absolute; vertical-align: middle; margin-top: 20px; margin-left: -47px; border: 0px; background: none; font-style: normal; font-variant: normal; font-weight: bold; font-stretch: normal; font-size: 12px; line-height: normal; font-family: Arial; text-align: center; color: rgb(57, 204, 204); padding: 0px; -webkit-appearance: none;">
                                </div>

                                <div class="knob-label">Online</div>
                            </div>
                            <!-- ./col -->
                            <div class="col-xs-4 text-center">
                                <div style="display:inline;width:60px;height:60px;">
                                    <canvas width="60" height="60"></canvas>
                                    <input type="text" class="knob" data-readonly="true" value="30" data-width="60"
                                           data-height="60" data-fgcolor="#39CCCC" readonly="readonly"
                                           style="width: 34px; height: 20px; position: absolute; vertical-align: middle; margin-top: 20px; margin-left: -47px; border: 0px; background: none; font-style: normal; font-variant: normal; font-weight: bold; font-stretch: normal; font-size: 12px; line-height: normal; font-family: Arial; text-align: center; color: rgb(57, 204, 204); padding: 0px; -webkit-appearance: none;">
                                </div>

                                <div class="knob-label">In-Store</div>
                            </div>
                            <!-- ./col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.box-footer -->
                </div>
                <!-- /.box -->
            </section>
            <!-- right col -->
        </div>
        <!-- /.row (main row) -->

    </section>
@endsection