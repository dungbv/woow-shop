<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
    <title>@yield('title') | Woow-Shop</title>
    <link rel="shortcut icon" href="{{ asset('client/images/favicon.ico') }}">
    @include('layouts.client-partial.style')
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
@include('layouts.client-partial.mobile_header')
<div id="wrapper" class="wide-wrap">
    <div class="offcanvas-overlay"></div>
    <header class="header-container header-type-classic header-navbar-classic header-scroll-resize">
        @include('layouts.client-partial.topbar')
        <div class="navbar-container">
            <div class="navbar navbar-default navbar-scroll-fixed">
                <div class="navbar-default-wrap">
                    <div class="container">
                        <div class="row">
                            <div class="navbar-default-col">
                                <div class="navbar-wrap">
                                    @include('layouts.client-partial.navbar_header')
                                    @include('layouts.client-partial.menu')
                                    @include('layouts.client-partial.header_right')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @include('layouts.client-partial.header_search')
            </div>
        </div>
    </header>
    @yield('content')
    @include('layouts.client-partial.footer')
</div>
@include('layouts.client-partial.modal.modal_login')
@include('layouts.client-partial.modal.modal_register')
@include('layouts.client-partial.modal.user_lost_pass')
@include('layouts.client-partial.minicart_side')
@include('layouts.client-partial.script')
</body>
</html>