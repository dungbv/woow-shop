<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar" style="height: auto;">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ asset('backend/dist/img/user2-160x160.jpg') }}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{ Auth::user()->name }}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->
        <form action="javascript:;" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu tree" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            <li class="active">
                <a href="{{ route('admin.dashboard') }}">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                </a>
            </li>
            <li class="treeview">
                <a href="javascript:;">
                    <i class="fa fa-pie-chart"></i>
                    <span>{{ __('CMS Manager') }}</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="javascript:;"><i class="fa fa-circle-o"></i> {{ __('CMS About') }}</a></li>
                    <li><a href="javascript:;"><i class="fa fa-circle-o"></i> {{ __('CMS Contact') }}</a></li>
                    <li><a href="javascript:;"><i class="fa fa-circle-o"></i> {{ __('CMS Article') }}</a></li>
                    <li><a href="javascript:;"><i class="fa fa-circle-o"></i> {{ __('CMS Page') }}</a></li>
                    <li><a href="javascript:;"><i class="fa fa-circle-o"></i> {{ __('CMS Tag') }}</a></li>
                    <li><a href="javascript:;"><i class="fa fa-circle-o"></i> {{ __('CMS Feedback') }}</a></li>
                    <li><a href="javascript:;"><i class="fa fa-circle-o"></i> {{ __('CMS Service') }}</a></li>
                    <li><a href="javascript:;"><i class="fa fa-circle-o"></i> {{ __('CMS Portfolio') }}</a></li>
                    <li><a href="javascript:;"><i class="fa fa-circle-o"></i> {{ __('CMS Partner') }}</a></li>
                    <li><a href="javascript:;"><i class="fa fa-circle-o"></i> {{ __('CMS Testimonial') }}</a></li>
                    <li><a href="javascript:;"><i class="fa fa-circle-o"></i> {{ __('CMS Slide') }}</a></li>
                    <li><a href="javascript:;"><i class="fa fa-circle-o"></i> {{ __('CMS Galerry') }}</a></li>
                    <li><a href="javascript:;"><i class="fa fa-circle-o"></i> {{ __('CMS FAQ') }}</a></li>
                    <li><a href="javascript:;"><i class="fa fa-circle-o"></i> {{ __('CMS Categories') }}</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="javascript:;">
                    <i class="fa fa-line-chart"></i>
                    <span>{{ __('Product Manager') }}</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="javascript:;"><i class="fa fa-circle-o"></i> {{ __('Products') }}</a></li>
                    <li><a href="javascript:;"><i class="fa fa-circle-o"></i> {{ __('Product Screenshots') }}</a></li>
                    <li><a href="javascript:;"><i class="fa fa-circle-o"></i> {{ __('Product Banner') }}</a></li>
                    <li><a href="javascript:;"><i class="fa fa-circle-o"></i> {{ __('Product Video') }}</a></li>
                    <li><a href="javascript:;"><i class="fa fa-circle-o"></i> {{ __('Product Audio') }}</a></li>
                    <li><a href="javascript:;"><i class="fa fa-circle-o"></i> {{ __('Product Versions') }}</a></li>
                    <li><a href="javascript:;"><i class="fa fa-circle-o"></i> {{ __('Product Features') }}</a></li>
                    <li><a href="javascript:;"><i class="fa fa-circle-o"></i> {{ __('Product Categories') }}</a></li>
                    <li><a href="javascript:;"><i class="fa fa-circle-o"></i> {{ __('Product Supplier') }}</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="javascript:;">
                    <i class="fa fa-user"></i>
                    <span>{{ __('User Manager') }}</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="javascript:;"><i class="fa fa-circle-o"></i> {{ __('Roles') }}</a></li>
                    <li><a href="javascript:;"><i class="fa fa-circle-o"></i> {{ __('Permission') }}</a></li>
                    <li><a href="javascript:;"><i class="fa fa-circle-o"></i> {{ __('Member') }}</a></li>
                    <li><a href="javascript:;"><i class="fa fa-circle-o"></i> {{ __('Administrator') }}</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="javascript:;">
                    <i class="fa fa-cc-paypal"></i>
                    <span>{{ __('Sales Manager') }}</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="javascript:;"><i class="fa fa-circle-o"></i> {{ __('Order Manager') }}</a></li>
                    <li><a href="javascript:;"><i class="fa fa-circle-o"></i> {{ __('Inventory Management') }}</a></li>
                    <li><a href="javascript:;"><i class="fa fa-circle-o"></i> {{ __('Menu Management') }}</a></li>
                    <li><a href="javascript:;"><i class="fa fa-circle-o"></i> {{ __('Pay Management') }}</a></li>
                    <li><a href="javascript:;"><i class="fa fa-circle-o"></i> {{ __('Desk Manager') }}</a></li>
                    <li><a href="javascript:;"><i class="fa fa-circle-o"></i> {{ __('Real-time reporting') }}</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="javascript:;">
                    <i class="fa fa-cc-paypal"></i>
                    <span>{{ __('CRM Manager') }}</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="javascript:;"><i class="fa fa-circle-o"></i> {{ __('Marketing Campaign') }}</a></li>
                    <li><a href="javascript:;"><i class="fa fa-circle-o"></i> {{ __('Event Management') }}</a></li>
                    <li><a href="javascript:;"><i class="fa fa-circle-o"></i> {{ __('Mass Mailing') }}</a></li>
                    <li><a href="javascript:;"><i class="fa fa-circle-o"></i> {{ __('Contacts Information') }}</a></li>
                    <li><a href="javascript:;"><i class="fa fa-circle-o"></i> {{ __('Manage Appointments') }}</a></li>
                    <li><a href="javascript:;"><i class="fa fa-circle-o"></i> {{ __('Manage Notes') }}</a></li>
                    <li><a href="javascript:;"><i class="fa fa-circle-o"></i> {{ __('Opporturnity pipeline') }}</a></li>
                    <li><a href="javascript:;"><i class="fa fa-circle-o"></i> {{ __('Call Center') }}</a></li>
                    <li><a href="javascript:;"><i class="fa fa-circle-o"></i> {{ __('Potential Management (Leads)') }}
                        </a></li>
                    <li><a href="javascript:;"><i class="fa fa-circle-o"></i> {{ __('Working schedule') }}</a></li>
                    <li><a href="javascript:;"><i class="fa fa-circle-o"></i> {{ __('Quotation') }}</a></li>
                    <li><a href="javascript:;"><i class="fa fa-circle-o"></i> {{ __('Business Intelligence') }}</a></li>
                    <li><a href="javascript:;"><i class="fa fa-circle-o"></i> {{ __('Reports and information boards') }}
                        </a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="javascript:;">
                    <i class="fa fa-cc-paypal"></i>
                    <span>{{ __('Human resources management, salary') }}</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="javascript:;"><i class="fa fa-circle-o"></i> {{ __('Catalog Manager') }}</a></li>
                    <li><a href="javascript:;"><i
                                    class="fa fa-circle-o"></i> {{ __('Employee Information management') }}</a></li>
                    <li><a href="javascript:;"><i class="fa fa-circle-o"></i> {{ __('Attendance Management') }}</a></li>
                    <li><a href="javascript:;"><i class="fa fa-circle-o"></i> {{ __('Salary Management') }}</a></li>
                    <li><a href="javascript:;"><i class="fa fa-circle-o"></i> {{ __('Insurance Management, Mode') }}</a>
                    </li>
                    <li><a href="javascript:;"><i class="fa fa-circle-o"></i> {{ __('Recruitment Manager') }}</a></li>
                    <li><a href="javascript:;"><i class="fa fa-circle-o"></i> {{ __('Training Management') }}</a></li>
                    <li><a href="javascript:;"><i class="fa fa-circle-o"></i> {{ __('Assessment Management') }}</a></li>
                    <li><a href="javascript:;"><i class="fa fa-circle-o"></i> {{ __('Budget Management') }}</a></li>
                    <li><a href="javascript:;"><i class="fa fa-circle-o"></i> {{ __('Operational') }}</a></li>
                    <li><a href="javascript:;"><i class="fa fa-circle-o"></i> {{ __('Reporting System') }}</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="javascript:;">
                    <i class="fa fa-cogfa fa-cog"></i>
                    <span>{{ __('System Administrator') }}</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="javascript:;"><i class="fa fa-circle-o"></i> {{ __('General Configuration') }}</a></li>
                    <li><a href="javascript:;"><i class="fa fa-circle-o"></i> {{ __('Configure Interface') }}</a></li>
                    <li><a href="javascript:;"><i class="fa fa-circle-o"></i> {{ __('Application Configuration') }}</a>
                    </li>
                    <li><a href="javascript:;"><i class="fa fa-circle-o"></i> {{ __('Application Setting') }} (
                            <small>{{ __('key => value') }} </small>
                            )</a></li>
                </ul>
            </li>
            <li class="header">{{ __('EXTENTIONS') }}</li>
            <li><a href="javascript:;"><i class="fa fa-circle-o text-red"></i> <span>{{ __('Model Generator') }}</span></a>
            </li>
            <li><a href="javascript:;"><i class="fa fa-circle-o text-red"></i>
                    <span>{{ __('CRUD Generator') }}</span></a></li>
            <li><a href="javascript:;"><i class="fa fa-circle-o text-yellow"></i> <span>{{ __('API Generator') }}</span></a>
            </li>
            <li><a href="{{ url('api-tester') }}"><i class="fa fa-circle-o text-yellow"></i> <span>{{ __('API Test') }}</span></a>
            </li>
            <li><a href="javascript:;"><i class="fa fa-circle-o text-aqua"></i> <span>{{ __('BACKUP Manager') }}</span></a>
            </li>
            <li><a href="javascript:;"><i class="fa fa-circle-o text-aqua"></i> <span>{{ __('Log Viewer') }}</span></a>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>