<script type='text/javascript' src='{{ asset('client/js/jquery.js') }}'></script>
<script type='text/javascript' src='{{ asset('client/js/jquery-migrate.min.js') }}'></script>
<script type='text/javascript' src='{{ asset('client/js/jquery.themepunch.tools.min.js') }}'></script>
<script type='text/javascript' src='{{ asset('client/js/jquery.themepunch.revolution.min.js') }}'></script>
<script type='text/javascript' src='{{ asset('client/js/easing.min.js') }}'></script>
<script type='text/javascript' src='{{ asset('client/js/imagesloaded.pkgd.min.js') }}'></script>
<script type='text/javascript' src='{{ asset('client/js/bootstrap.min.js') }}'></script>
<script type='text/javascript' src='{{ asset('client/js/superfish-1.7.4.min.js') }}'></script>
<script type='text/javascript' src='{{ asset('client/js/jquery.appear.min.js') }}'></script>
<script type='text/javascript' src='{{ asset('client/js/script.js') }}'></script>
<script type='text/javascript' src='{{ asset('client/js/swatches-and-photos.js') }}'></script>
<script type='text/javascript' src='{{ asset('client/js/jquery.cookie.min.js') }}'></script>
<script type='text/javascript' src='{{ asset('client/js/jquery.prettyPhoto.js') }}'></script>
<script type='text/javascript' src='{{ asset('client/js/jquery.prettyPhoto.init.min.js') }}'></script>
<script type='text/javascript' src='{{ asset('client/js/jquery.selectBox.min.js') }}'></script>
<script type='text/javascript' src='{{ asset('client/js/jquery.touchSwipe.min.js') }}'></script>
<script type='text/javascript' src='{{ asset('client/js/jquery.transit.min.js') }}'></script>
<script type='text/javascript' src='{{ asset('client/js/jquery.carouFredSel.js') }}'></script>
<script type='text/javascript' src='{{ asset('client/js/jquery.magnific-popup.js') }}'></script>
<script type='text/javascript' src='{{ asset('client/js/isotope.pkgd.min.js') }}'></script>

<script type='text/javascript' src='{{ asset('client/js/extensions/revolution.extension.video.min.js') }}'></script>
<script type='text/javascript' src='{{ asset('client/js/extensions/revolution.extension.slideanims.min.js') }}'></script>
<script type='text/javascript' src='{{ asset('client/js/extensions/revolution.extension.actions.min.js') }}'></script>
<script type='text/javascript' src='{{ asset('client/js/extensions/revolution.extension.layeranimation.min.js') }}'></script>
<script type='text/javascript' src='{{ asset('client/js/extensions/revolution.extension.kenburn.min.js') }}'></script>
<script type='text/javascript' src='{{ asset('client/js/extensions/revolution.extension.navigation.min.js') }}'></script>
<script type='text/javascript' src='{{ asset('client/js/extensions/revolution.extension.migration.min.js') }}'></script>
<script type='text/javascript' src='{{ asset('client/js/extensions/revolution.extension.parallax.min.js') }}'></script>
<script type="text/javascript">

    var tpj=jQuery;

    var revapi7;
    tpj(document).ready(function() {
        if(tpj("#rev_slider").revolution == undefined){
            revslider_showDoubleJqueryError("#rev_slider");
        }else{
            revapi7 = tpj("#rev_slider").show().revolution({
                sliderType:"standard",
                sliderLayout:"fullwidth",
                dottedOverlay:"none",
                delay:9000,
                navigation: {
                    keyboardNavigation:"off",
                    keyboard_direction: "horizontal",
                    mouseScrollNavigation:"off",
                    onHoverStop:"on",
                    touch:{
                        touchenabled:"on",
                        swipe_threshold: 75,
                        swipe_min_touches: 50,
                        swipe_direction: "horizontal",
                        drag_block_vertical: false
                    }
                    ,
                    arrows: {
                        style:"gyges",
                        enable:true,
                        hide_onmobile:true,
                        hide_under:600,
                        hide_onleave:true,
                        hide_delay:200,
                        hide_delay_mobile:1200,
                        tmp:'',
                        left: {
                            h_align:"left",
                            v_align:"center",
                            h_offset:30,
                            v_offset:0
                        },
                        right: {
                            h_align:"right",
                            v_align:"center",
                            h_offset:30,
                            v_offset:0
                        }
                    }
                    ,
                    bullets: {
                        enable:true,
                        hide_onmobile:true,
                        hide_under:600,
                        style:"hephaistos",
                        hide_onleave:true,
                        hide_delay:200,
                        hide_delay_mobile:1200,
                        direction:"horizontal",
                        h_align:"center",
                        v_align:"bottom",
                        h_offset:0,
                        v_offset:30,
                        space:5,
                        tmp:''
                    }
                },
                gridwidth:1170,
                gridheight:600,
                lazyType:"smart",
                parallax: {
                    type:"mouse",
                    origo:"slidercenter",
                    speed:2000,
                    levels:[2,3,4,5,6,7,12,16,10,50],
                },
                shadow:0,
                spinner:"off",
                stopLoop:"off",
                stopAfterLoops:-1,
                stopAtSlide:-1,
                shuffle:"off",
                autoHeight:"off",
                disableProgressBar:"on",
                hideThumbsOnMobile:"off",
                hideSliderAtLimit:0,
                hideCaptionAtLimit:0,
                hideAllCaptionAtLilmit:0,
                startWithSlide:0,
                debugMode:false,
                fallbacks: {
                    simplifyAll:"off",
                    nextSlideOnWindowFocus:"off",
                    disableFocusListener:false,
                }
            });
        }
    });	/*ready*/
</script>
@yield('script')