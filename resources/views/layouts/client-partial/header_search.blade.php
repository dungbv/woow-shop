<div class="header-search-overlay hide">
    <div class="container">
        <div class="header-search-overlay-wrap">
            <form class="searchform">
                <input type="search" class="searchinput" name="s" autocomplete="off" value="" placeholder="Search..."/>
            </form>
            <button type="button" class="close">
                <span aria-hidden="true" class="fa fa-times"></span>
                <span class="sr-only">Close</span>
            </button>
        </div>
    </div>
</div>