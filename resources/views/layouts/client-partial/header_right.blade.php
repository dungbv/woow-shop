<div class="header-right">
    <div class="navbar-search">
        <a class="navbar-search-button" href="#">
            <i class="fa fa-search"></i>
        </a>
        <div class="search-form-wrap show-popup hide"></div>
    </div>
    <div class="navbar-minicart navbar-minicart-topbar">
        <div class="navbar-minicart">
            <a class="minicart-link" href="#">
                <span class="minicart-icon">
                    <i class="fa fa-shopping-cart"></i>
                    <span>0</span>
                </span>
            </a>
        </div>
    </div>
    <div class="navbar-wishlist">
        <a class="wishlist" href="wishlist.html">
            <i class="fa fa-heart-o"></i>
        </a>
    </div>
</div>