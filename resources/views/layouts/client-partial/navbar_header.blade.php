<div class="navbar-header">
    <button type="button" class="navbar-toggle">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar bar-top"></span>
        <span class="icon-bar bar-middle"></span>
        <span class="icon-bar bar-bottom"></span>
    </button>
    <a class="navbar-search-button search-icon-mobile" href="#">
        <i class="fa fa-search"></i>
    </a>
    <a class="cart-icon-mobile" href="#">
        <i class="elegant_icon_bag"></i><span>0</span>
    </a>
    <a class="navbar-brand" href="./">
        <img class="logo" alt="WOOW" src="{{ asset('client/images/logo.png') }}">
        <img class="logo-fixed" alt="WOOW" src="{{ asset('client/images/logo-fixed.png') }}">
        <img class="logo-mobile" alt="WOOW" src="{{ asset('client/images/logo-mobile.png') }}">
    </a>
</div>