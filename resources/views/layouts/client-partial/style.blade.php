<link rel='stylesheet' href='{{ asset('client/css/settings.css') }}' type='text/css' media='all'/>
<link rel='stylesheet' href='{{ asset('client/css/bootstrap.min.css') }}' type='text/css' media='all'/>
<link rel='stylesheet' href='{{ asset('client/css/swatches-and-photos.css') }}' type='text/css' media='all'/>
<link rel='stylesheet' href='{{ asset('client/css/prettyPhoto.css') }}' type='text/css' media='all'/>
<link rel='stylesheet' href='{{ asset('client/css/jquery.selectBox.css') }}' type='text/css' media='all'/>
<link rel='stylesheet' href='{{ asset('client/css/font-awesome.min.css') }}' type='text/css' media='all'/>
<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Karla:400,400italic,700,700italic%7CCrimson+Text:400,400italic,600,600italic,700,700italic' type='text/css' media='all'/>
<link rel='stylesheet' href='//fonts.googleapis.com/css?family=Montserrat:400,700' type='text/css' media='all'/>
<link rel='stylesheet' href='{{ asset('client/css/elegant-icon.css') }}' type='text/css' media='all'/>
<link rel='stylesheet' href='{{ asset('client/css/style.css') }}' type='text/css' media='all'/>
<link rel='stylesheet' href='{{ asset('client/css/commerce.css') }}' type='text/css' media='all'/>
<link rel='stylesheet' href='{{ asset('client/css/custom.css') }}' type='text/css' media='all'/>
<link rel='stylesheet' href='{{ asset('client/css/magnific-popup.css') }}' type='text/css' media='all'/>
<link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Lora%3A400%2C700%7CCrimson+Text:400,400italic,600,600italic,700,700italic' type='text/css' media='all'/>
@yield('style')