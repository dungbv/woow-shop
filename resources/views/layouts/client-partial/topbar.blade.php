<div class="topbar">
    <div class="container topbar-wap">
        <div class="row">
            <div class="col-sm-6 col-left-topbar">
                <div class="left-topbar">
                    Shop unique and handmade items directly
                    <a href="#">About<i class="fa fa-long-arrow-right"></i></a>
                </div>
            </div>
            <div class="col-sm-6 col-right-topbar">
                <div class="right-topbar">
                    <div class="user-login">
                        <ul class="nav top-nav">
                            <li><a data-rel="loginModal" href="#"> Login </a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>