<nav class="collapse navbar-collapse primary-navbar-collapse">
    <ul class="nav navbar-nav primary-nav">
        <li class="current-menu-item menu-item-has-children dropdown">
            <a href="./" class="dropdown-hover">
                <span class="underline">Home</span> <span class="caret"></span>
            </a>
            <ul class="dropdown-menu">
                <li><a href="home-v2.html">Home v2</a></li>
                <li><a href="home-v3.html">Home v3</a></li>
                <li><a href="home-v4.html">Home v4</a></li>
            </ul>
        </li>
        <li class="menu-item-has-children megamenu megamenu-fullwidth dropdown">
            <a href="shop.html" class="dropdown-hover">
                <span class="underline">Shop</span> <span class="caret"></span>
            </a>
            <ul class="dropdown-menu">
                <li class="mega-col-3">
                    <h3 class="megamenu-title">Women <span class="caret"></span></h3>
                    <ul class="dropdown-menu">
                        <li><a href="shop-by-category.html">Maecenas</a></li>
                        <li><a href="shop-by-category.html">Nulla</a></li>
                        <li><a href="shop-by-category.html">Donec</a></li>
                        <li><a href="shop-by-category.html">Aliquam</a></li>
                    </ul>
                </li>
                <li class="mega-col-3">
                    <h3 class="megamenu-title">Brands <span class="caret"></span></h3>
                    <ul class="dropdown-menu">
                        <li><a href="shop-by-category.html">Evans</a></li>
                        <li><a href="shop-by-category.html">Adesso</a></li>
                        <li><a href="shop-by-category.html">Crocs</a></li>
                        <li><a href="shop-by-category.html">Carvela</a></li>
                    </ul>
                </li>
                <li class="mega-col-3">
                    <h3 class="megamenu-title">Features <span class="caret"></span></h3>
                    <ul class="dropdown-menu">
                        <li><a href="shop-fullwidth.html">Products Style 2</a></li>
                        <li><a href="shop-detail-2.html">Single Product Style 2</a></li>
                        <li><a href="shop-masonry.html">Shop Masonry</a></li>
                        <li><a href="my-account.html">My Account</a></li>
                        <li><a href="cart.html">Cart</a></li>
                        <li><a href="cart-empty.html">Empty Cart</a></li>
                        <li><a href="wishlist.html">Wishlist</a></li>
                    </ul>
                </li>
                <li class="mega-col-3">
                    <h3 class="megamenu-title">Mega Menu</h3>
                    <div class="megamenu-sidebar">
                        <div class="widget widget_products commerce">
                            <ul class="product_list_widget">
                                <li>
                                    <a href="shop-detail-1.html">
                                        <img src="{{ asset('client/images/products/thumb/product_60x60.jpg') }}" alt="2a"/>
                                        <span class="product-title">Cras rhoncus duis viverra</span>
                                    </a>
                                    <span class="amount">&pound;12.00</span>
                                    &ndash;
                                    <span class="amount">&pound;20.00</span>
                                </li>
                                <li>
                                    <a href="shop-detail-1.html">
                                        <img src="{{ asset('client/images/products/thumb/product_60x60.jpg') }}" alt="3a"/>
                                        <span class="product-title">Morbi sed egestas</span>
                                    </a>
                                    <span class="amount">&pound;123.00</span>
                                </li>
                                <li>
                                    <a href="shop-detail-1.html">
                                        <img src="{{ asset('client/images/products/thumb/product_60x60.jpg') }}" alt="4a"/>
                                        <span class="product-title">Creamy Spring Pasta</span>
                                    </a>
                                    <span class="amount">&pound;321.00</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </li>
            </ul>
        </li>
        <li><a href="collection.html"><span class="underline">Collections</span></a></li>
        <li class="menu-item-has-children dropdown">
            <a href="#" class="dropdown-hover">
                <span class="underline">Blog</span> <span class="caret"></span>
            </a>
            <ul class="dropdown-menu">
                <li><a href="blog-default.html">Blog Default</a></li>
                <li><a href="blog-center.html">Blog Center</a></li>
                <li><a href="blog-masonry.html">Blog Masonry</a></li>
            </ul>
        </li>
        <li class="menu-item-has-children dropdown">
            <a href="#" class="dropdown-hover">
                <span class="underline">Pages</span> <span class="caret"></span>
            </a>
            <ul class="dropdown-menu">
                <li><a href="about-us.html">About us</a></li>
                <li><a href="contact-us.html">Contact Us</a></li>
                <li><a href="faq.html">FAQ</a></li>
                <li><a href="404.html">404</a></li>
            </ul>
        </li>
    </ul>
</nav>