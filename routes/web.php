<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});
Route::get('/contact-us', function () {
    return view('client.contact_us');
});
Route::get('/about-us', function () {
    return view('client.about_us');
});
Route::get('/blog', function () {
    return view('client.blog');
});
Route::get('/blog-detail', function () {
    return view('client.blog_detail');
});
Route::get('/collection', function () {
    return view('client.collection');
});
Route::get('/product', function () {
    return view('client.product');
});
Route::get('/product-detail', function () {
    return view('client.product_detail');
});
Route::get('/cart', function () {
    return view('client.cart');
});
Route::get('/cart-empty', function () {
    return view('client.cart_empty');
});
Route::get('/wishlist', function () {
    return view('client.wishlist');
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
