<?php
/**
 * Created by PhpStorm.
 * User: tongd
 * Date: 11/03/2018
 * Time: 10:26 SA
 */

Route::group(['namespace' => 'Auth'], function() {
    Route::get('/login', 'AdminLoginController@showLoginForm')->name('admin.login');
    Route::post('/login', 'AdminLoginController@login')->name('admin.login.submit');
    Route::post('/logout', 'AdminLoginController@logout')->name('admin.logout.submit');
    Route::post('/forgot-password', 'AdminLoginController@logout')->name('admin.forgot-password.submit');

    Route::post('/password/email', 'AdminForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
    Route::get('/password/reset/{token}', 'AdminResetPasswordController@showResetForm')->name('admin.password.reset');
    Route::post('/password/reset', 'AdminResetPasswordController@reset')->name('admin.password.admin-request');
});
Route::get('/', 'Admin\AdminController@index')->name('admin.dashboard');
